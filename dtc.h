#ifndef _DTC_H
#define _DTC_H

#include <stdint.h>

#include "aldl.h"

typedef struct aldl_dtc {
  const char     *description;
  const char     *location;
  uint16_t        sae_dtc;
  uint16_t        oem_dtc;
  int             historical;
  aldl_time_t     timestamp;
  aldl_cvid_t     vehicle_module;
} aldl_dtc_t;

APIEXPORT int aldl_dtc_read(aldl_session_t*, aldl_cvid_t, aldl_dtc_t**, size_t*);
APIEXPORT int aldl_dtc_clear(aldl_session_t*, aldl_cvid_t);

#endif