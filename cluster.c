#include <stdint.h>
#include <string.h>

#include "aldl.h"
#include "gmseedkey.h"
#include "osal/osal.h"

APIEXPORT int aldl_cluster_text(aldl_session_t *s, char *str) {
  if(!s->driver)
    return ALDL_ENODRIVER;

  if(!s->driver->cluster_text)
    return ALDL_EBADVEHICLE;

  return s->driver->cluster_text(s, str);
}
