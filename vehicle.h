#ifndef _VEHICLE_H
#define _VEHICLE_H

#include <stdint.h>

#include "aldl.h"

typedef struct {
  int   make;
  char *name;
  char *region;
} aldl_make_t;

typedef struct {
  aldl_cvid_t     vehicle;
  char           *model_name;
  char           *model_code;
  uint16_t        model_year;
} aldl_model_t;

#endif