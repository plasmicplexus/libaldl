#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "aldl.h"

#pragma pack(1)
typedef struct {
  uint32_t magic_number;
  uint16_t version_major;
  uint16_t version_minor;
  int32_t  thiszone;
  uint32_t sigfigs;
  uint32_t snaplen;
  uint32_t network;
} pcap_hdr_t;
#pragma pack()

#pragma pack(1)
typedef struct {
  uint32_t ts_sec;
  uint32_t ts_usec;
  uint32_t incl_len;
  uint32_t orig_len;
} pcaprec_hdr_t;
#pragma pack()

APIEXPORT int aldl_pcap_start(aldl_session_t *s, const char *path) {
  int ret;

  pcap_hdr_t hdr;

  hdr.magic_number  = 0xA1B2C3D4;
  hdr.version_major = 2;
  hdr.version_minor = 4;
  hdr.thiszone      = 0;
  hdr.sigfigs       = 0;
  hdr.snaplen       = 256;
  hdr.network       = 147;

  ret = osal_pcap_open(&s->pcap, path);
  if(ret)
    return ALDL_EPCAPOPEN;

  osal_pcap_write(s->pcap, &hdr, sizeof(hdr));

  return 0;
}

APIEXPORT int aldl_pcap_stop(aldl_session_t *s) {
  assert(!s->pcap, ALDL_EINVAL);
  osal_pcap_close(s->pcap);
  s->pcap = 0;
  return 0;
}

int pcap_write(aldl_session_t *s, void *buf, size_t len, aldl_time_t ts) {
  ssize_t out;

  char pcapbuf[173 + sizeof(pcaprec_hdr_t)];
  pcaprec_hdr_t *hdr = (void*) pcapbuf;

  aldl_time_t us = ts ? ts : osal_micros();

  if(len > 173)
    return ALDL_EINVAL;

  if(!s->pcap)
    return ALDL_EINVAL;

  hdr->ts_sec   = us / 1000000;
  hdr->ts_usec  = us % 1000000;
  hdr->incl_len = len;
  hdr->orig_len = len;

  memcpy(pcapbuf + sizeof(pcaprec_hdr_t), buf, len);

  out = osal_pcap_write(s->pcap, pcapbuf, len + sizeof(pcaprec_hdr_t));

  return out == len + sizeof(pcaprec_hdr_t) ? 0 : ALDL_EIO;
}
