#include <stdarg.h>
#include <string.h>

#include "aldl.h"
#include "radio.h"

APIEXPORT int aldl_send_radstat(aldl_session_t *s, int stat, ...) {
  int ret;
  va_list ap;

  if(!s->driver)
    return ALDL_ENODRIVER;

  if(!s->driver->send_radstat)
    return ALDL_EBADVEHICLE;

  va_start(ap, stat);
  ret = s->driver->send_radstat(s, stat, ap);
  va_end(ap);
  return ret;
}
