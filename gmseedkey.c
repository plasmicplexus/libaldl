#include <stdint.h>
#include <stdio.h>

#include "aldl.h"
#include "gmseedkey.h"
#include "osal/osal.h"

extern char stgterm_bin[3328];

#pragma pack(1)
struct op {
  uint8_t  opcode;
  uint16_t oper;
};

struct alg {
  uint8_t id;
  struct op op[4];
};
#pragma pack()

static uint16_t do_op(int opcode, uint16_t oper, uint16_t val) {
  uint8_t l = oper & 0xFF;
  uint8_t h = oper >> 8;

  switch(opcode) {
    case 0x14:
      return val + oper;
    case 0x2A:
      return h >= l ? ~val : ~val - 1;
    case 0x4C:
      return (val << h) | (val >> (16 - h));
    case 0x6B:
      return (val >> l) | (val << (16 - l));
    case 0x7E:
      val = (val >> 8) | (val << 8);
      return val + (l > h ? (l << 8) | h : oper);
    case 0x98:
      return val - oper;
    default:
      return val;
  }
}

APIEXPORT int gmseedkey(int alg, uint16_t seed, uint16_t *key) {
  int i;

  struct op *o;

  uint16_t val = seed;

  char *stgterm = NULL;

#ifdef CONF_INCLUDE_GMSEEDKEY
  struct alg *matrix = (void*) stgterm;
#endif

  if(alg == GM_ALG_CLUSTER) {
    val = (val >> 8) | (val << 8);
    *key = 0xBE87 + ~val;
    return 0;
  }

  if(alg > 255)
    return ALDL_EINVAL;

#ifdef CONF_INCLUDE_GMSEEDKEY
  for(i = 0; i < 4; i++) {
    o = &matrix[alg].op[i];
    val = do_op(o->opcode, be16toh(o->oper), val);
  }
#else
  return ALDL_EMISSINGFEAT;
#endif

  *key = val;
  return 0;
}
