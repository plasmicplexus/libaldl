#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "../aldl.h"
#include "../gmseedkey.h"
#include "../radio.h"
#include "holden.h"

#define NDTCDEF \
  (sizeof(dtcdefs) / sizeof(struct dtcdef))

struct dtcdef {
  aldl_cvid_t    vmod;
  uint16_t       saedtc;
  uint16_t       oemdtc;
  uint8_t        hist;

  uint8_t  tbl;
  uint16_t bit  : 3;
  uint16_t offs : 13;

  char *desc;
};

struct table {
  int tbl;
  int len;
  char data[170];
};

/* DTC definition table */
const struct dtcdef dtcdefs[] = {
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 0, .bit = 0, .oemdtc = 13,     .hist = 0, .desc = "Ambient temperature sensor voltage too high"                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 0, .bit = 1, .oemdtc = 14,     .hist = 0, .desc = "Ambient temperature sensor voltage too low"                                            },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 0, .bit = 2, .oemdtc = 15,     .hist = 0, .desc = "In car temperature sensor voltage too high"                                            },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 0, .bit = 3, .oemdtc = 16,     .hist = 0, .desc = "In car temperature sensor voltage too low"                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 0, .bit = 4, .oemdtc = 17,     .hist = 0, .desc = "Evaporative temperature sensor voltage too high"                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 0, .bit = 5, .oemdtc = 18,     .hist = 0, .desc = "Evaporative temporature sensor voltage too low"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 1, .bit = 0, .oemdtc = 35,     .hist = 0, .desc = "No serial data from PCM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 1, .bit = 1, .oemdtc = 36,     .hist = 0, .desc = "No serial data from BCM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 1, .bit = 2, .oemdtc = 19,     .hist = 0, .desc = "Sun load sensor error"                                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 1, .bit = 3, .oemdtc = 39,     .hist = 0, .desc = "RAM error"                                                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 1, .bit = 4, .oemdtc = 38,     .hist = 0, .desc = "EEPROM checksum error"                                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 1, .bit = 5, .oemdtc = 37,     .hist = 0, .desc = "ROM checksum error"                                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 1, .bit = 6, .oemdtc = 40,     .hist = 0, .desc = "Air mix door motor driver error"                                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 1, .bit = 7, .oemdtc = 41,     .hist = 0, .desc = "Solenoid driver error"                                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 2, .bit = 0, .oemdtc = 43,     .hist = 0, .desc = "Drivers air mix door motor feedback circuit voltage too low"                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 2, .bit = 1, .oemdtc = 44,     .hist = 0, .desc = "Drivers air mix door motor feedback circuit voltage too high"                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 2, .bit = 2, .oemdtc = 45,     .hist = 0, .desc = "Passengers air mix door motor feedback circuit voltage too low"                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 2, .bit = 3, .oemdtc = 46,     .hist = 0, .desc = "Passengers air mix door motor feedback circuit voltage too high"                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 2, .bit = 4, .oemdtc = 47,     .hist = 0, .desc = "Drivers air mix door minimum calibration error"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 2, .bit = 5, .oemdtc = 48,     .hist = 0, .desc = "Drivers air mix door maximum calibration error"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 2, .bit = 6, .oemdtc = 49,     .hist = 0, .desc = "Passengers air mix door minimum calibration error"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 2, .bit = 7, .oemdtc = 50,     .hist = 0, .desc = "Passengers air mix door maximum calibration error"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 4, .bit = 0, .oemdtc = 13,     .hist = 1, .desc = "Ambient temperature sensor voltage too high"                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 4, .bit = 1, .oemdtc = 14,     .hist = 1, .desc = "Ambient temperature sensor voltage too low"                                            },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 4, .bit = 2, .oemdtc = 15,     .hist = 1, .desc = "In car temperature sensor voltage too high"                                            },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 4, .bit = 3, .oemdtc = 16,     .hist = 1, .desc = "In car temperature sensor voltage too low"                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 4, .bit = 4, .oemdtc = 17,     .hist = 1, .desc = "Evaporative temperature sensor voltage too high"                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 4, .bit = 5, .oemdtc = 18,     .hist = 1, .desc = "Evaporative temporature sensor voltage too low"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 5, .bit = 0, .oemdtc = 35,     .hist = 1, .desc = "No serial data from PCM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 5, .bit = 1, .oemdtc = 36,     .hist = 1, .desc = "No serial data from BCM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 5, .bit = 2, .oemdtc = 19,     .hist = 1, .desc = "Sun load sensor error"                                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 5, .bit = 3, .oemdtc = 39,     .hist = 1, .desc = "RAM error"                                                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 5, .bit = 4, .oemdtc = 38,     .hist = 1, .desc = "EEPROM checksum error"                                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 5, .bit = 5, .oemdtc = 37,     .hist = 1, .desc = "ROM checksum error"                                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 5, .bit = 6, .oemdtc = 40,     .hist = 1, .desc = "Air mix door motor driver error"                                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 5, .bit = 7, .oemdtc = 41,     .hist = 1, .desc = "Solenoid driver error"                                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 6, .bit = 0, .oemdtc = 43,     .hist = 1, .desc = "Drivers air mix door motor feedback circuit voltage too low"                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 6, .bit = 1, .oemdtc = 44,     .hist = 1, .desc = "Drivers air mix door motor feedback circuit voltage too high"                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 6, .bit = 2, .oemdtc = 45,     .hist = 1, .desc = "Passengers air mix door motor feedback circuit voltage too low"                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 6, .bit = 3, .oemdtc = 46,     .hist = 1, .desc = "Passengers air mix door motor feedback circuit voltage too high"                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 6, .bit = 4, .oemdtc = 47,     .hist = 1, .desc = "Drivers air mix door minimum calibration error"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 6, .bit = 5, .oemdtc = 48,     .hist = 1, .desc = "Drivers air mix door maximum calibration error"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 6, .bit = 6, .oemdtc = 49,     .hist = 1, .desc = "Passengers air mix door minimum calibration error"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_HVAC),    .tbl = 2, .offs = 6, .bit = 7, .oemdtc = 50,     .hist = 1, .desc = "Passengers air mix door maximum calibration error"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 0, .bit = 0, .oemdtc = 24,     .hist = 0, .desc = "EEPROM checksum error"                                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 0, .bit = 1, .oemdtc = 25,     .hist = 0, .desc = "ROM checksum error"                                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 1, .bit = 0, .oemdtc = 16,     .hist = 0, .desc = "Petrol level sender stuck"                                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 1, .bit = 1, .oemdtc = 17,     .hist = 0, .desc = "LPG level sender stuck"                                                                },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 1, .bit = 2, .oemdtc = 18,     .hist = 0, .desc = "Passenger seat sensor fault"                                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 1, .bit = 3, .oemdtc = 19,     .hist = 0, .desc = "Incorrect SDM detected"                                                                },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 1, .bit = 5, .oemdtc = 21,     .hist = 0, .desc = "Trip computer switch voltage low"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 1, .bit = 6, .oemdtc = 22,     .hist = 0, .desc = "Trip computer switch button stuck"                                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 2, .bit = 0, .oemdtc = 8,      .hist = 0, .desc = "No serial data from ABS/ETC"                                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 2, .bit = 1, .oemdtc = 9,      .hist = 0, .desc = "No serial data from BCM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 2, .bit = 2, .oemdtc = 10,     .hist = 0, .desc = "No serial data from OCC"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 2, .bit = 3, .oemdtc = 11,     .hist = 0, .desc = "No serial data from PCM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 2, .bit = 4, .oemdtc = 12,     .hist = 0, .desc = "No serial data from SDM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 2, .bit = 5, .oemdtc = 13,     .hist = 0, .desc = "No instrument poll from BCM"                                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 2, .bit = 6, .oemdtc = 14,     .hist = 0, .desc = "No serial data"                                                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 2, .bit = 7, .oemdtc = 15,     .hist = 0, .desc = "No serial data from audio system"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 3, .bit = 1, .oemdtc = 1,      .hist = 0, .desc = "Petrol level sender voltage low"                                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 3, .bit = 2, .oemdtc = 2,      .hist = 0, .desc = "Petrol level sender voltage intermittent"                                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 3, .bit = 3, .oemdtc = 3,      .hist = 0, .desc = "Petrol level sender voltage high"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 3, .bit = 4, .oemdtc = 4,      .hist = 0, .desc = "LPG level sender voltage high"                                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 3, .bit = 5, .oemdtc = 5,      .hist = 0, .desc = "LPG level sender voltage low"                                                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 3, .bit = 6, .oemdtc = 6,      .hist = 0, .desc = "LPG level sender voltage intermittent"                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 4, .bit = 0, .oemdtc = 24,     .hist = 1, .desc = "EEPROM checksum error"                                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 4, .bit = 1, .oemdtc = 25,     .hist = 1, .desc = "ROM checksum error"                                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 5, .bit = 0, .oemdtc = 16,     .hist = 1, .desc = "Petrol level sender stuck"                                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 5, .bit = 1, .oemdtc = 17,     .hist = 1, .desc = "LPG level sender stuck"                                                                },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 5, .bit = 2, .oemdtc = 18,     .hist = 1, .desc = "Passenger seat sensor fault"                                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 5, .bit = 3, .oemdtc = 19,     .hist = 1, .desc = "Incorrect SDM detected"                                                                },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 5, .bit = 5, .oemdtc = 21,     .hist = 1, .desc = "Trip computer switch voltage low"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 5, .bit = 6, .oemdtc = 22,     .hist = 1, .desc = "Trip computer switch button stuck"                                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 6, .bit = 0, .oemdtc = 8,      .hist = 1, .desc = "No serial data from ABS/ETC"                                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 6, .bit = 1, .oemdtc = 9,      .hist = 1, .desc = "No serial data from BCM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 6, .bit = 2, .oemdtc = 10,     .hist = 1, .desc = "No serial data from OCC"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 6, .bit = 3, .oemdtc = 11,     .hist = 1, .desc = "No serial data from PCM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 6, .bit = 4, .oemdtc = 12,     .hist = 1, .desc = "No serial data from SDM"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 6, .bit = 5, .oemdtc = 13,     .hist = 1, .desc = "No instrument poll from BCM"                                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 6, .bit = 6, .oemdtc = 14,     .hist = 1, .desc = "No serial data"                                                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 6, .bit = 7, .oemdtc = 15,     .hist = 1, .desc = "No serial data from audio system"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 7, .bit = 1, .oemdtc = 1,      .hist = 1, .desc = "Petrol level sender voltage low"                                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 7, .bit = 2, .oemdtc = 2,      .hist = 1, .desc = "Petrol level sender voltage intermittent"                                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 7, .bit = 3, .oemdtc = 3,      .hist = 1, .desc = "Petrol level sender voltage high"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 7, .bit = 4, .oemdtc = 4,      .hist = 1, .desc = "LPG level sender voltage high"                                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 7, .bit = 5, .oemdtc = 5,      .hist = 1, .desc = "LPG level sender voltage low"                                                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_CLUSTER), .tbl = 3, .offs = 7, .bit = 6, .oemdtc = 6,      .hist = 1, .desc = "LPG level sender voltage intermittent"                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 0, .bit = 0, .oemdtc = 10,     .hist = 0, .desc = "Fascia button jammed"                                                                  },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 0, .bit = 1, .oemdtc = 11,     .hist = 0, .desc = "Steering wheel remote button jammed"                                                   },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 0, .bit = 2, .oemdtc = 12,     .hist = 0, .desc = "Rear remote button jammed"                                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 1, .bit = 1, .oemdtc = 21,     .hist = 0, .desc = "CD mechanism error"                                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 1, .bit = 2, .oemdtc = 22,     .hist = 0, .desc = "CD play error"                                                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 1, .bit = 4, .oemdtc = 24,     .hist = 0, .desc = "CD loading error"                                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 1, .bit = 5, .oemdtc = 25,     .hist = 0, .desc = "CD defect"                                                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 1, .bit = 6, .oemdtc = 26,     .hist = 0, .desc = "CD general error"                                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 2, .bit = 0, .oemdtc = 30,     .hist = 0, .desc = "Internal bus failure"                                                                  },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 2, .bit = 3, .oemdtc = 33,     .hist = 0, .desc = "Single communication bus failure"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 2, .bit = 4, .oemdtc = 34,     .hist = 0, .desc = "Multi communication bus failure"                                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 2, .bit = 5, .oemdtc = 35,     .hist = 0, .desc = "Fascia communication bus failure"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 2, .bit = 6, .oemdtc = 36,     .hist = 0, .desc = "DSP communications bus failure"                                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 3, .bit = 0, .oemdtc = 40,     .hist = 0, .desc = "No BCM serial data"                                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 3, .bit = 1, .oemdtc = 41,     .hist = 0, .desc = "No instrument serial data"                                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_RADIO),   .tbl = 1, .offs = 3, .bit = 2, .oemdtc = 42,     .hist = 0, .desc = "External drive bus failure"                                                            },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 0, .bit = 0, .oemdtc = 17,     .hist = 0, .desc = "Driver airbag circuit short to battery"                                                },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 0, .bit = 1, .oemdtc = 18,     .hist = 0, .desc = "Driver airbag circuit short to ground"                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 0, .bit = 2, .oemdtc = 19,     .hist = 0, .desc = "Driver airbag circuit capacitance too high"                                            },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 0, .bit = 3, .oemdtc = 20,     .hist = 0, .desc = "Driver airbag circuit capacitance too low"                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 0, .bit = 4, .oemdtc = 21,     .hist = 0, .desc = "Driver airbag circuit resistance too high"                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 0, .bit = 5, .oemdtc = 22,     .hist = 0, .desc = "Driver airbag circuit resistance too low"                                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 0, .bit = 6, .oemdtc = 247,    .hist = 0, .desc = "Driver airbag circuit power stage error"                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 1, .bit = 1, .oemdtc = 33,     .hist = 0, .desc = "Passenger airbag circuit short to battery"                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 1, .bit = 1, .oemdtc = 34,     .hist = 0, .desc = "Passenger airbag circuit short to ground"                                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 1, .bit = 2, .oemdtc = 35,     .hist = 0, .desc = "Passenger airbag circuit capacitance too high"                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 1, .bit = 3, .oemdtc = 36,     .hist = 0, .desc = "Passenger airbag circuit capacitance too low"                                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 1, .bit = 4, .oemdtc = 37,     .hist = 0, .desc = "Passenger airbag circuit resistance too high"                                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 1, .bit = 5, .oemdtc = 38,     .hist = 0, .desc = "Passenger airbag circuit resistance too low"                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 1, .bit = 6, .oemdtc = 247,    .hist = 0, .desc = "Passenger airbag circuit power stage error"                                            },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 2, .bit = 0, .oemdtc = 49,     .hist = 0, .desc = "Left-hand pretensioner circuit short to battery"                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 2, .bit = 1, .oemdtc = 50,     .hist = 0, .desc = "Left-hand pretensioner circuit short to ground"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 2, .bit = 2, .oemdtc = 51,     .hist = 0, .desc = "Left-hand pretensioner circuit capacitance too high"                                   },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 2, .bit = 3, .oemdtc = 52,     .hist = 0, .desc = "Left-hand pretensioner circuit capacitance too low"                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 2, .bit = 4, .oemdtc = 53,     .hist = 0, .desc = "Left-hand pretensioner circuit resistance too high"                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 2, .bit = 5, .oemdtc = 54,     .hist = 0, .desc = "Left-hand pretensioner circuit resistence too low"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 2, .bit = 6, .oemdtc = 247,    .hist = 0, .desc = "Left-hand pretensioner circuit power stage error"                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 3, .bit = 0, .oemdtc = 65,     .hist = 0, .desc = "Right-hand pretensioner circuit short to battery"                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 3, .bit = 1, .oemdtc = 66,     .hist = 0, .desc = "Right-hand pretensioner circuit short to ground"                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 3, .bit = 2, .oemdtc = 67,     .hist = 0, .desc = "Right-hand pretensioner circuit capacitance too high"                                  },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 3, .bit = 3, .oemdtc = 68,     .hist = 0, .desc = "Right-hand pretensioner circuit capacitance too low"                                   },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 3, .bit = 4, .oemdtc = 69,     .hist = 0, .desc = "Right-hand pretensioner circuit resistance too high"                                   },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 3, .bit = 5, .oemdtc = 70,     .hist = 0, .desc = "Right-hand pretensioner circuit resistence too low"                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 3, .bit = 6, .oemdtc = 247,    .hist = 0, .desc = "Right-hand pretensioner circuit power stage error"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 4, .bit = 0, .oemdtc = 81,     .hist = 0, .desc = "Left-hand side-airbag circuit short to battery"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 4, .bit = 1, .oemdtc = 82,     .hist = 0, .desc = "Left-hand side-airbag circuit short to ground"                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 4, .bit = 2, .oemdtc = 83,     .hist = 0, .desc = "Left-hand side-airbag circuit capacitance too high"                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 4, .bit = 3, .oemdtc = 84,     .hist = 0, .desc = "Left-hand side-airbag circuit capacitance too low"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 4, .bit = 4, .oemdtc = 85,     .hist = 0, .desc = "Left-hand side-airbag circuit resistance too high"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 4, .bit = 5, .oemdtc = 86,     .hist = 0, .desc = "Left-hand side-airbag circuit resistence too low"                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 4, .bit = 6, .oemdtc = 247,    .hist = 0, .desc = "Left-hand side-airbag circuit power stage error"                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 5, .bit = 0, .oemdtc = 97,     .hist = 0, .desc = "Right-hand side-airbag circuit short to battery"                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 5, .bit = 1, .oemdtc = 98,     .hist = 0, .desc = "Right-hand side-airbag circuit short to ground"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 5, .bit = 2, .oemdtc = 99,     .hist = 0, .desc = "Right-hand side-airbag circuit capacitance too high"                                   },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 5, .bit = 3, .oemdtc = 100,    .hist = 0, .desc = "Right-hand side-airbag circuit capacitance too low"                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 5, .bit = 4, .oemdtc = 101,    .hist = 0, .desc = "Right-hand side-airbag circuit resistance too high"                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 5, .bit = 5, .oemdtc = 102,    .hist = 0, .desc = "Right-hand side-airbag circuit resistence too low"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 5, .bit = 6, .oemdtc = 247,    .hist = 0, .desc = "Right-hand side-airbag circuit power stage error"                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 7, .bit = 0, .oemdtc = 129,    .hist = 0, .desc = "Left peripheral acceleration sensor line fault"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 7, .bit = 1, .oemdtc = 130,    .hist = 0, .desc = "Right peripheral acceleration sensor line fault"                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 7, .bit = 4, .oemdtc = 131,    .hist = 0, .desc = "Left peripheral acceleration sensor communication fault"                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 7, .bit = 5, .oemdtc = 132,    .hist = 0, .desc = "Right peripheral acceleration sensor communication fault"                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 8, .bit = 0, .oemdtc = 133,    .hist = 0, .desc = "Left peripheral acceleration sensor identification fault"                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 8, .bit = 1, .oemdtc = 134,    .hist = 0, .desc = "Right peripheral acceleration sensor identification fault"                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 8, .bit = 4, .oemdtc = 135,    .hist = 0, .desc = "Left peripheral acceleration sensor hardware fault"                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_SRS),     .tbl = 2, .offs = 8, .bit = 5, .oemdtc = 136,    .hist = 0, .desc = "Right peripheral acceleration sensor hardware fault"                                   },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 0, .bit = 0, .saedtc = 0x0123, .hist = 0, .desc = "TPS voltage high"                                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 0, .bit = 1, .saedtc = 0x0121, .hist = 0, .desc = "TPS stuck"                                                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 0, .bit = 2, .saedtc = 0x0400, .hist = 0, .desc = "EGR flow fault indicated"                                                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 0, .bit = 3, .saedtc = 0x1628, .hist = 0, .desc = "PCM error ECT"                                                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 0, .bit = 4, .saedtc = 0x1116, .hist = 0, .desc = "ECT signal voltage unstable"                                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 0, .bit = 5, .saedtc = 0x0118, .hist = 0, .desc = "ECT signal voltage high"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 0, .bit = 6, .saedtc = 0x0117, .hist = 0, .desc = "ECT signal voltage low"                                                                },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 0, .bit = 7, .saedtc = 0x0134, .hist = 0, .desc = "No right hand oxygen sensor signal"                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 1, .bit = 0, .saedtc = 0x0405, .hist = 0, .desc = "EGR position fault"                                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 1, .bit = 1, .saedtc = 0x1810, .hist = 0, .desc = "Transmission fluid pressure manual valve position switch assembly circuit malfunction" },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 1, .bit = 3, .saedtc = 0x0111, .hist = 0, .desc = "IAT signal voltage unstable"                                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 1, .bit = 4, .saedtc = 0x0112, .hist = 0, .desc = "IAT signal voltage low"                                                                },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 1, .bit = 5, .saedtc = 0x0502, .hist = 0, .desc = "No vehicle speed signal"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 1, .bit = 6, .saedtc = 0x0113, .hist = 0, .desc = "IAT signal voltage high"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 1, .bit = 7, .saedtc = 0x0122, .hist = 0, .desc = "TPS voltage low"                                                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 2, .bit = 2, .saedtc = 0x0507, .hist = 0, .desc = "Vacuum leak"                                                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 2, .bit = 3, .saedtc = 0x0506, .hist = 0, .desc = "Idle speed error"                                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 2, .bit = 4, .saedtc = 0x0107, .hist = 0, .desc = "Barometric pressure sensor voltage low"                                                },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 2, .bit = 5, .saedtc = 0x0108, .hist = 0, .desc = "Barometric pressure sensor voltage high"                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 2, .bit = 6, .saedtc = 0x0101, .hist = 0, .desc = "MAF sensor out of range"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 2, .bit = 7, .saedtc = 0x1255, .hist = 0, .desc = "Theft deterrent signal missing"                                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 3, .bit = 0, .saedtc = 0x0374, .hist = 0, .desc = "18X reference signal missing"                                                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 3, .bit = 1, .saedtc = 0x1372, .hist = 0, .desc = "No reference pulses while cranking"                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 3, .bit = 2, .saedtc = 0x0132, .hist = 0, .desc = "Right hand oxygen sensor signal voltage high"                                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 3, .bit = 3, .saedtc = 0x0131, .hist = 0, .desc = "Right hand oxygen sensor signal voltage low"                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 3, .bit = 5, .saedtc = 0x1361, .hist = 0, .desc = "Ignition bypass circuit fault"                                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 3, .bit = 6, .saedtc = 0x1351, .hist = 0, .desc = "Ignition electronic spark timing circuit fault"                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 4, .bit = 0, .saedtc = 0x0171, .hist = 0, .desc = "Lean condition under load"                                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 4, .bit = 1, .saedtc = 0x1627, .hist = 0, .desc = "PCM A/D conversion error"                                                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 4, .bit = 2, .saedtc = 0x0561, .hist = 0, .desc = "System voltage unstable"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 4, .bit = 3, .saedtc = 0x0563, .hist = 0, .desc = "System voltage too high"                                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 4, .bit = 4, .saedtc = 0x0560, .hist = 0, .desc = "System voltage too high long time"                                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 4, .bit = 5, .saedtc = 0x0601, .hist = 0, .desc = "PCM memory"                                                                            },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 4, .bit = 6, .saedtc = 0x0341, .hist = 0, .desc = "Cam crank sensor signal intermittent"                                                  },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 4, .bit = 7, .saedtc = 0x0342, .hist = 0, .desc = "Camshaft position sensor signal missing"                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 5, .bit = 0, .saedtc = 0x0152, .hist = 0, .desc = "Left hand oxygen sensor signal voltage high"                                           },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 5, .bit = 1, .saedtc = 0x0151, .hist = 0, .desc = "Left hand oxygen sensor signal voltage low"                                            },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 5, .bit = 2, .saedtc = 0x0154, .hist = 0, .desc = "No left hand oxygen sensor signal"                                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 5, .bit = 3, .saedtc = 0x0327, .hist = 0, .desc = "Left knock sensor circuit fault"                                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 5, .bit = 4, .saedtc = 0x0332, .hist = 0, .desc = "Right knock sensor circuit fault"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 5, .bit = 5, .saedtc = 0x0713, .hist = 0, .desc = "Transmission fluid temperature signal voltage high"                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 5, .bit = 6, .saedtc = 0x0712, .hist = 0, .desc = "Transmission fluid temperature signal voltage low"                                     },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 5, .bit = 7, .saedtc = 0x0200, .hist = 0, .desc = "Injector voltage monitor fault"                                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 6, .bit = 1, .saedtc = 0x0748, .hist = 0, .desc = "Pressure control solenoid current error"                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 6, .bit = 2, .saedtc = 0x0503, .hist = 0, .desc = "Vehicle speed sensor intermittent signal"                                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 6, .bit = 3, .saedtc = 0x0717, .hist = 0, .desc = "Engine speed low"                                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 6, .bit = 4, .saedtc = 0x0741, .hist = 0, .desc = "Torque converter clutch stuck on"                                                      },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 6, .bit = 6, .saedtc = 0x0740, .hist = 0, .desc = "Torque converter clutch enable solenoid circuit fault"                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 6, .bit = 7, .saedtc = 0x0785, .hist = 0, .desc = "3-2 downshift control solenoid circuit fault"                                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 7, .bit = 0, .saedtc = 0x1860, .hist = 0, .desc = "Torque converter clutch pulse width modulation solenoid fault"                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 7, .bit = 1, .saedtc = 0x0730, .hist = 0, .desc = "2-3 shift solenoid B circuit voltage high"                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 7, .bit = 2, .saedtc = 0x0753, .hist = 0, .desc = "1-2 shift solenoid A circuit voltage high"                                             },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 7, .bit = 3, .saedtc = 0x0218, .hist = 0, .desc = "Transmission fluid overtemperature"                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 7, .bit = 4, .saedtc = 0x0170, .hist = 0, .desc = "Long term fuel trim delta high"                                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 7, .bit = 6, .saedtc = 0x0173, .hist = 0, .desc = "Short term fuel trim delta high"                                                       },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 7, .bit = 7, .saedtc = 0x0562, .hist = 0, .desc = "System voltage low"                                                                    },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 8, .bit = 0, .saedtc = 0x1064, .hist = 0, .desc = "Low speed fan no BCM response"                                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 8, .bit = 1, .saedtc = 0x1643, .hist = 0, .desc = "LPG fuel control valve PWM out of range"                                               },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 8, .bit = 4, .saedtc = 0x0757, .hist = 0, .desc = "2-3 shift solenoid B circuit voltage low"                                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 8, .bit = 5, .saedtc = 0x0756, .hist = 0, .desc = "1-2 shift solenoid A circuit voltage low"                                              },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 8, .bit = 6, .saedtc = 0x1870, .hist = 0, .desc = "Transmission slipping"                                                                 },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 8, .bit = 7, .saedtc = 0x1642, .hist = 0, .desc = "LPG enable signal out of range"                                                        },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 9, .bit = 2, .saedtc = 0x0443, .hist = 0, .desc = "Canister purge function fault"                                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 9, .bit = 3, .saedtc = 0x0446, .hist = 0, .desc = "Canister purge circuit fault"                                                          },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 9, .bit = 4, .saedtc = 0x0530, .hist = 0, .desc = "A/C refrigerant pressure sensor circuit malfunction"                                   },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 9, .bit = 5, .saedtc = 0x1571, .hist = 0, .desc = "Requested torque out of range"                                                         },
  { .vmod = aldl_cvid_module(aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY), ALDL_MODULE_PCM),     .tbl = 6, .offs = 9, .bit = 7, .saedtc = 0x0325, .hist = 0, .desc = "DSNEF(Digital Signal-to-Noise Enhancement Filter) system fault"                        }
};

/* VY cluster known EEPROM tables and their sizes */
/*
 * 0x01  2 0x02  2 0x03 20 0x0D 10
 * 0x17  1 0x18  1 0x19  1 0x1A  4
 * 0x1C  1 0x1D  1 0x1E  1 0x1F  1
 * 0x20  1 0x21 20 0x2B 10 0x36  1
 * 0x37  1 0x38  4 0x3A  1 0x3B  1
 * 0x3C  1 0x3D  1 0x3E  9 0x47  1
 * 0x48  5 0x4D  1 0x4E  1 0x4F  1
 * 0x50  1 0x51  1 0x52  4 0x53  1
 * 0x54  1 0x55  4 0x56  4 0x57  4
 * 0x58  4 0x59  4 0x5A  4 0x5B  4
 * 0x5D  4 0x5E  4 0x5F  4 0x60  4
 * 0x61  8 0x65  4 0x66  1 0x67  1
 * 0x68  1 0x69  1 0x6A  1 0x6B  1
 * 0x6C  1 0x6D  1 0x6E  1 0x6F  2
 * 0x70  2 0x71  1 0x72  1 0x73  1
 * 0x74  1 0x75  1 0x76  4 0x77  4
 * 0x78  4 0x79  4 0x7A  1 0x7B  2
 * 0x7C  2 0x7D  2 0x7E  1 0x7F  1
 * 0x80  5 0x85  1 0x86  5 0x8B  1
 * 0x8C  1 0x8D  1 0x8E  1 0x8F  1
 * 0x90  5 0x95  1 0x96  1 0x97  1
 * 0x98  1 0x99  1 0x9A  8 0x9E  2
 * 0x9F  4 0xA0  1 0xA1  5 0xA6  1
 * 0xA7  4 0xA8  2 0xA9  1 0xAA  4
 * 0xAB 15 0xBA  9 0xC3  5 0xC8  5
 * 0xCD  2 0xCF  6 0xD5  5 0xDA  1
 * 0xDB  1 0xDC  4 0xDD  4 0xDE  2
 * 0xE0  3 0xE3  1 0xE4  1 0xE5  1
 * 0xE6 10 0xF0  1 0xF1  5 0xF6  1
 * 0xF7  8 0xFB  1 0xFC  2 0xFD  1
 * 0xFE  2 0xFF  2
 */

/* high-level configuration tables */
/*
 * static const aldl_dev_conf_t conf_table_vy_cluster =  {
 *   .options = {
 *     { 0x01, ALDL_CONF_CLUSTER_CAL_SPEEDO,      "cal_speedo",      2, ALDL_INT    },
 *     { 0x02, ALDL_CONF_CLUSTER_CAL_TACHO,       "cal_tacho",       2, ALDL_INT    },
 *     { 0x17, ALDL_CONF_CLUSTER_PETROL_CAPACITY, "petrol_capacity", 1, ALDL_INT    },
 *     { 0x18, ALDL_CONF_CLUSTER_PETROL_DAMPING,  "petrol_damping",  1, ALDL_INT    },
 *     { 0x1C, ALDL_CONF_CLUSTER_PETROL_LOW,      "petrol_low",      1, ALDL_INT    },
 *     { 0x1D, ALDL_CONF_CLUSTER_PETROL_VLOW,     "petrol_vlow",     1, ALDL_INT    },
 *     { 0x37, ALDL_CONF_CLUSTER_DRL,             "drl",             1, ALDL_INT    },
 *     { 0x3E, ALDL_CONF_CLUSTER_TEMP_MAP,        "temp_map",        9, ALDL_INTARR },
 *     { 0x47, ALDL_CONF_CLUSTER_TEMP_HIGH,       "temp_high",       1, ALDL_INT    },
 *     { 0x48, ALDL_CONF_CLUSTER_BRIGHT_MAP,      "bright_map",      5, ALDL_INTARR },
 *     { 0x50, ALDL_CONF_CLUSTER_POLICE,          "police",          1, ALDL_BOOL   },
 *     { 0x52, ALDL_CONF_CLUSTER_SERVICE,         "service",         4, ALDL_INT    },
 *     { 0x60, ALDL_CONF_CLUSTER_SERIALNO,        "serialno",        4, ALDL_INT    },
 *     { 0x65, ALDL_CONF_CLUSTER_GM_PARTNO,       "gm_partno",       4, ALDL_INT    },
 *     { 0x6D, ALDL_CONF_CLUSTER_STOPWATCH,       "stopwatch",       1, ALDL_BOOL   },
 *     { 0x67, ALDL_CONF_CLUSTER_DTG,             "dtg",             1, ALDL_BOOL   },
 *     { 0x69, ALDL_CONF_CLUSTER_RADSTAT,         "radstat",         1, ALDL_BOOL   },
 *     { 0x73, ALDL_CONF_CLUSTER_BRIGHT_GAUGE,    "bright_gauge",    1, ALDL_INT    },
 *     { 0x74, ALDL_CONF_CLUSTER_BRIGHT_PTR,      "bright_ptr",      1, ALDL_INT    },
 *     { 0x75, ALDL_CONF_CLUSTER_BRIGHT_LCD,      "bright_lcd",      1, ALDL_INT    },
 *     { 0x7E, ALDL_CONF_CLUSTER_AUTOTRANS,       "autotrans",       1, ALDL_BOOL   },
 *     { 0x86, ALDL_CONF_CLUSTER_OVERSPEED,       "overspeed",       5, ALDL_INTARR },
 *     { 0xAA, ALDL_CONF_CLUSTER_VIN,             "vin",             4, ALDL_INT    },
 *     { 0xDD, ALDL_CONF_CLUSTER_FUELCAL_PARTNO,  "fuelcal_partno",  4, ALDL_INT    },
 *     { 0xE3, ALDL_CONF_CLUSTER_CONTRAST_CENTER, "contrast_center", 1, ALDL_INT    },
 *     { 0xE4, ALDL_CONF_CLUSTER_CONTRAST_SIDE,   "contrast_side",   1, ALDL_INT    },
 *     { 0, 0, NULL, 0, 0 } [> null terminator - do not delete <]
 *   }
 * };
 */

static aldl_packet_t bcm_silence = {
  .id   = 0xF1,
  .len  = 1,
  .data = { 8, }
};

static int diagid(int module) {
  switch(module & 0xFFFFF) {
    case (ALDL_MODEL_COMMODORE_VT << 8) | ALDL_MODULE_BCM:        return ALDL_DEV_VT_BCM;
    case (ALDL_MODEL_COMMODORE_VT << 8) | ALDL_MODULE_PCM:        return ALDL_DEV_VT_PCM;
    case (ALDL_MODEL_COMMODORE_VT << 8) | ALDL_MODULE_ABS:        return ALDL_DEV_VT_ABS;
    case (ALDL_MODEL_COMMODORE_VT << 8) | ALDL_MODULE_SRS:        return ALDL_DEV_VT_SRS;
    case (ALDL_MODEL_COMMODORE_VX << 8) | ALDL_MODULE_BCM:        return ALDL_DEV_VX_BCM;
    case (ALDL_MODEL_COMMODORE_VX << 8) | ALDL_MODULE_PCM:        return ALDL_DEV_VX_PCM;
    case (ALDL_MODEL_COMMODORE_VX << 8) | ALDL_MODULE_ABS:        return ALDL_DEV_VX_ABS;
    case (ALDL_MODEL_COMMODORE_VX << 8) | ALDL_MODULE_SRS:        return ALDL_DEV_VX_SRS;
    case (ALDL_MODEL_COMMODORE_VY << 8) | ALDL_MODULE_TELEMATICS: return ALDL_DEV_VY_TELEMATICS;
    case (ALDL_MODEL_COMMODORE_VY << 8) | ALDL_MODULE_HVAC:       return ALDL_DEV_VY_CLIMATE;
    case (ALDL_MODEL_COMMODORE_VY << 8) | ALDL_MODULE_RADIO:      return ALDL_DEV_VY_RADIO;
    case (ALDL_MODEL_COMMODORE_VY << 8) | ALDL_MODULE_BCM:        return ALDL_DEV_VY_BCM;
    case (ALDL_MODEL_COMMODORE_VY << 8) | ALDL_MODULE_CLUSTER:    return ALDL_DEV_VY_CLUSTER;
    case (ALDL_MODEL_COMMODORE_VY << 8) | ALDL_MODULE_PIM:        return ALDL_DEV_VY_PIM;
    case (ALDL_MODEL_COMMODORE_VY << 8) | ALDL_MODULE_PCM:        return ALDL_DEV_VY_PCM;
    case (ALDL_MODEL_COMMODORE_VY << 8) | ALDL_MODULE_ABS:        return ALDL_DEV_VY_ABS;
    default:
      return 0;
  }
}

static int dtc_read(aldl_session_t *s, aldl_cvid_t module, aldl_dtc_t **dtclist, size_t *ndtclist) {
  int i, j;
  int ret;

  uint8_t tbl;
  uint8_t tbl_needed[32];
  int ntbl_needed = 0;
  struct table *tables;

  int ndtc = 0;
  aldl_dtc_t *dtc, *dtcs;

  aldl_packet_t resp;

  int id = diagid(module);
  assert(!id, ALDL_EBADVEHICLE);

  /* parse DTC definitions and determine required tables */
  memset(tbl_needed, 0, sizeof(tbl_needed));
  for(i = 0; i < NDTCDEF; i++) {
    if(aldl_cvid_vehicleonly(dtcdefs[i].vmod) != s->conf.vehicle)
      continue;
    if(!(tbl_needed[dtcdefs[i].tbl >> 3] & (1 << i & 7)))
      ntbl_needed++;
    tbl_needed[dtcdefs[i].tbl >> 3] |= 1 << (dtcdefs[i].tbl & 7);
  }

  tables = malloc(sizeof(struct table) * ntbl_needed);
  assert(!tables, ALDL_ENOMEM);

  /* retrieve the required tables */
  for(i = j = 0; i < 256; i++) {
    if(!(tbl_needed[i >> 3] & (i & 7)))
      continue;
    tbl = i;
    ret = aldl_send_wait(s, id, 1, &tbl, 1, &resp);
    if(ret)
      goto error;

    tables[j].tbl = i;
    tables[j].len = resp.len;
    memcpy(tables[j].data, resp.data, resp.len);
    j++;
  }

  /* pre-calculate memory required for returned DTC list */
  for(i = 0; i < ntbl_needed; i++) {
    for(j = 0; j < NDTCDEF; j++) {
      if(aldl_cvid_vehicleonly(dtcdefs[j].vmod) != s->conf.vehicle)
        continue;
      if(dtcdefs[j].tbl != tables[i].tbl)
        continue;
      /*
       * skip the +2 offset if we're reading from the PCM,
       * as it doesn't include the table request at the beginning of the response
       */
      if((tables[i].data[dtcdefs[j].offs + (id == 0xF7 ? 0 : 2)] & (1 << dtcdefs[j].bit)))
        ndtc++;
    }
  }

  dtcs = malloc(sizeof(aldl_dtc_t) * ndtc);
  if(!dtcs)
    goto error;

  dtc = dtcs;
  for(i = 0; i < ntbl_needed; i++) {
    for(j = 0; j < NDTCDEF; j++) {
      if(aldl_cvid_vehicleonly(dtcdefs[j].vmod) != s->conf.vehicle)
        continue;
      if(dtcdefs[j].tbl != tables[i].tbl)
        continue;
      if(!(tables[i].data[dtcdefs[j].offs + (id == 0xF7 ? 0 : 2)] & (1 << dtcdefs[j].bit)))
        continue;
      memset(dtc, 0, sizeof(aldl_dtc_t));
      dtc->description = dtcdefs[j].desc;
      dtc->sae_dtc     = dtcdefs[j].saedtc;
      dtc->oem_dtc     = dtcdefs[j].oemdtc;
      dtc->historical  = dtcdefs[j].hist;
      dtc++;
    }
  }

  free(tables);
  return 0;

error:
  free(tables);
  return ret;
}

static int dtc_clear(aldl_session_t *s, aldl_cvid_t module) {
  /* TODO: wait for a response */
  int id = diagid(module);
  assert(!id, ALDL_EBADVEHICLE);
  return aldl_send(s, id, 0x0A, NULL, 0);
}

static int silence_chatter(aldl_session_t *s, int en) {
  int ret;

  switch(aldl_model(s->conf.vehicle)) {
    case ALDL_MODEL_COMMODORE_VT:
    case ALDL_MODEL_COMMODORE_VX:
    case ALDL_MODEL_COMMODORE_VY:
    case ALDL_MODEL_COMMODORE_VZ:
      break;
    default:
      return ALDL_EBADVEHICLE;
  }

  if(en) {
    ret = aldl_register_txsched(s, CONF_SILENCE_INTERVAL, &bcm_silence, &bcm_silence, NULL);
    assert(ret, ret);
  } else {
    aldl_unregister_txsched(s, &bcm_silence);
  }

  return 0;
}

static int send_radstat(aldl_session_t *s, int stat, va_list ap) {
  int len = -1;;

  int x;
  int preset;
  double freq;

  aldl_packet_t p, rp;
  memset(&p, 0, sizeof(p));

  if(aldl_model(s->conf.vehicle) != ALDL_MODEL_COMMODORE_VY)
    if(aldl_model(s->conf.vehicle) != ALDL_MODEL_COMMODORE_VZ)
      return ALDL_EBADVEHICLE;

  p.data[0] = 9;
  p.data[3] = stat;

  switch(stat) {
    case ALDL_RADSTAT_VOLUME:
    case ALDL_RADSTAT_BASS:
    case ALDL_RADSTAT_TREBLE:
    case ALDL_RADSTAT_BOOST:
    case ALDL_RADSTAT_ECHO:
    case ALDL_RADSTAT_DELAY:
    case ALDL_RADSTAT_FADER:
    case ALDL_RADSTAT_BALANCE:
      x = va_arg(ap, int);
      p.data[4] = 0x02;
      p.data[5] = (x % 7);
      len = 6;
      break;
    case ALDL_RADSTAT_PAUSE:
    case ALDL_RADSTAT_MUTE:
    case ALDL_RADSTAT_MIDRANGE:
    case ALDL_RADSTAT_VIDEO:
    case ALDL_RADSTAT_CD:
    case ALDL_RADSTAT_TAPE:
    case ALDL_RADSTAT_RADIO:
    case ALDL_RADSTAT_VOICEIN:
    case ALDL_RADSTAT_INSERT_DISC:
    case ALDL_RADSTAT_REMOVE_DISC:
    case ALDL_RADSTAT_LOADING_DISC:
    case ALDL_RADSTAT_EJECTING_DISC:
    case ALDL_RADSTAT_EJECTING_ALL:
    case ALDL_RADSTAT_LOAD_ALL:
    case ALDL_RADSTAT_PLEASE_WAIT:
      break;
    case ALDL_RADSTAT_AM:
    case ALDL_RADSTAT_FM1:
    case ALDL_RADSTAT_FM2:
    case ALDL_RADSTAT_FMASM:
      preset = va_arg(ap, int);
      freq = va_arg(ap, double);
      p.data[4]  = 0x3C;
      p.data[5]  = 0x30 + (preset % 7);
      p.data[6]  = 0x30 + ((int) floor(freq / 100) % 10);
      p.data[7]  = 0x30 + ((int) floor(freq / 10)  % 10);
      p.data[8]  = 0x30 + ((int) floor(freq)       % 10);
      p.data[9]  = 0x30 + ((int) floor(freq * 10)  % 10);
      p.data[10] = 0x00;
      len = 11;
      break;
  }

  if(len < 0)
    return ALDL_EINVAL;

  return aldl_send_wait(s, 0xB9, 2, p.data, sizeof(p.data), &rp);
}

static int cluster_text(aldl_session_t *s, const char *str) {
  int ret;

  aldl_packet_t pkt;

  char buf[169] = { 0x00, 0x00, 0x10, 0x00, 0x00, 0x0A, 0x00, };

  if(aldl_model(s->conf.vehicle) != ALDL_MODEL_COMMODORE_VY)
    if(aldl_model(s->conf.vehicle) != ALDL_MODEL_COMMODORE_VZ)
      return ALDL_EBADVEHICLE;

  /*
   * TODO: cluster replies by adding 0xB9 to it's 0x21
   * status frame. We should listen for this...
   */

  strncpy(&buf[6], str, sizeof(buf) - 7);

  ret = aldl_send_wait(s, 0xB9, 2, buf, strlen(buf + 6) + 7, &pkt);
  assert(ret < 0, ALDL_EIO);

  return 0;
}

static int cluster_unlock(aldl_session_t *s) {
  int i;
  int ret;

  aldl_packet_t p, rp;
  memset(&p, 0, sizeof(p));

  uint16_t *seed = (void*) &rp.data[1];
  uint16_t key;

  if(aldl_model(s->conf.vehicle) != ALDL_MODEL_COMMODORE_VY)
    if(aldl_model(s->conf.vehicle) != ALDL_MODEL_COMMODORE_VZ)
      return ALDL_EBADVEHICLE;

  for(i = 0; i < s->conf.tx_retries; i++ ) {
    aldl_send(s, 0xF2, 0x11, p.data, 1);
    ret = aldl_wait_packet(s, 0xF2, &rp, 1000000);
    if(!ret)
      break;
  }

  assert(ret, ALDL_ENORESP);
  assert(rp.len != 3 || rp.data[0] != 0x11, ALDL_EBADRESP);

  gmseedkey(GM_ALG_CLUSTER, be16toh(*seed), &key);

  p.data[0] = 0x01;
  *((uint16_t*) &p.data[1]) = htobe16(key);

  ret = aldl_send_wait(s, 0xF2, 0x11, p.data, 3, &rp);

  assert(ret, ALDL_ENORESP);
  assert(rp.len != 3 || rp.data[0] != 0x11, ALDL_EBADRESP);
  assert(be16toh(*((uint16_t*) &rp.data[1])) != key, ALDL_EBADRESP);

  return 0;
}

/*
 * static int cluster_read_eeprom_table(aldl_session_t *s, int tab, void *buf, size_t *len) {
 *   int i;
 *   int ret;
 * 
 *   aldl_packet_t req, resp;
 * 
 *   if(aldl_model(s->conf.vehicleid) != ALDL_MODEL_COMMODORE_VY)
 *     if(aldl_model(s->conf.vehicleid) != ALDL_MODEL_COMMODORE_VZ)
 *       return ALDL_EBADVEHICLE;
 * 
 *   memset(&req, 0, sizeof(req));
 *   req.data[1] = tab;
 * 
 *   for(i = 0; i < NTABLES; i++) {
 *     if(cluster_tables[i][0] == tab)
 *       break;
 *   }
 * 
 *   if(i == NTABLES || *len < cluster_tables[i][1])
 *     return ALDL_EINVAL;
 * 
 *   *len = cluster_tables[i][1];
 * 
 *   for(i = 0; i < s->conf.tx_retries; i++) {
 *     ret = aldl_send_wait(s, 0xF2, 0x22, req.data, 2, &resp);
 *     if(ret)
 *       continue;
 * 
 *     if(resp.data[0] != 0x22 || resp.len - 4 != *len) {
 *       ret = ALDL_EBADRESP;
 *       continue;
 *     }
 * 
 *     if((uint8_t) resp.data[3] != tab) {
 *       ret = ALDL_EBADRESP;
 *       continue;
 *     }
 * 
 *     memcpy(buf, &resp.data[4], *len);
 *     ret = 0;
 *     break;
 *   }
 * 
 *   return ret;
 * }
 */

/*
 * static int cluster_write_eeprom_table(aldl_session_t *s, int tab, void *buf, size_t len) {
 *   int i;
 *   int ret;
 * 
 *   aldl_packet_t req, resp;
 * 
 *   if(aldl_model(s->conf.vehicleid) != ALDL_MODEL_COMMODORE_VY)
 *     if(aldl_model(s->conf.vehicleid) != ALDL_MODEL_COMMODORE_VZ)
 *       return ALDL_EBADVEHICLE;
 * 
 *   memset(&req, 0, sizeof(req));
 *   req.data[0] = 0x55;
 *   req.data[2] = tab;
 * 
 *   for(i = 0; i < NTABLES; i++) {
 *     if(cluster_tables[i][0] == tab)
 *       break;
 *   }
 * 
 *   if(i == NTABLES || len != cluster_tables[i][1])
 *     return ALDL_EINVAL;
 * 
 *   memcpy(&req.data[3], buf, len);
 * 
 *   for(i = 0; i < s->conf.tx_retries; i++) {
 *     ret = aldl_send_wait(s, 0xF2, 0x20, req.data, len + 3, &resp);
 *     if(ret)
 *       continue;
 * 
 *     if(resp.data[0] != 0x20 || resp.len != 4 || (uint8_t) resp.data[3] != tab) {
 *       ret = ALDL_EBADRESP;
 *       continue;
 *     }
 * 
 *     if(resp.data[1] == 8) {
 *       ret = aldl_cluster_unlock(s);
 *       continue;
 *     }
 * 
 *     if(resp.data[1] != 1) {
 *       ret = ALDL_EBADRESP;
 *       continue;
 *     }
 * 
 *     ret = 0;
 *     break;
 *   }
 * 
 *   return ret;
 * }
 */

struct driver driver_holden = {
  .cluster_text    = &cluster_text,
  .dtc_read        = &dtc_read,
  .dtc_clear       = &dtc_clear,
  .send_radstat    = &send_radstat,
  .silence_chatter = &silence_chatter,
  .models = {
    {
      .vehicle    = aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VT),
      .model_name = "Commodore",
      .model_code = "VT",
      .model_year = 1997
    },
    {
      .vehicle    = aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VX),
      .model_name = "Commodore",
      .model_code = "VX",
      .model_year = 2000
    },
    {
      .vehicle    = aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VY),
      .model_name = "Commodore",
      .model_code = "VY",
      .model_year = 2002
    },
    {
      .vehicle    = aldl_cvid(ALDL_MAKE_HOLDEN, ALDL_MODEL_COMMODORE_VZ),
      .model_name = "Commodore",
      .model_code = "VZ",
      .model_year = 2004
    },
    { .vehicle  = 0 }
  }
};
