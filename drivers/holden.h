#ifndef _DRIVERS_HOLDEN_H
#define _DRIVERS_HOLDEN_H

#include "../aldl.h"

enum HOLDEN_MODEL {
  ALDL_MODEL_COMMODORE_VR = 1,
  ALDL_MODEL_COMMODORE_VS = 2,
  ALDL_MODEL_COMMODORE_VT = 3,
  ALDL_MODEL_COMMODORE_VX = 4,
  ALDL_MODEL_COMMODORE_VY = 5,
  ALDL_MODEL_COMMODORE_VZ = 6
};

enum ALDL_DEV {
  ALDL_DEV_TESTER        = 0xF0,

  ALDL_DEV_VR_BCM        = 0xBD,
  ALDL_DEV_VR_SRS        = 0xF2,
  ALDL_DEV_VR_PCM        = 0xF4,
  ALDL_DEV_VR_ABS        = 0xF9,

  ALDL_DEV_VS_BCM        = 0xBD,
  ALDL_DEV_VS_SRS        = 0xF2,
  ALDL_DEV_VS_PCM_V8     = 0xF4,
  ALDL_DEV_VS_PCM_V6     = 0xF5,
  ALDL_DEV_VS_ABS        = 0xF9,

  ALDL_DEV_VT_BCM        = 0xF1,
  ALDL_DEV_VT_PCM        = 0xF5,
  ALDL_DEV_VT_ABS        = 0xF9,
  ALDL_DEV_VT_SRS        = 0xFA,

  ALDL_DEV_VX_BCM        = 0xF1,
  ALDL_DEV_VX_PCM        = 0xF7,
  ALDL_DEV_VX_ABS        = 0xF9,
  ALDL_DEV_VX_SRS        = 0xFB,

  ALDL_DEV_VY_TELEMATICS = 0xE4,
  ALDL_DEV_VY_CLIMATE    = 0xEA,
  ALDL_DEV_VY_RADIO      = 0xEB,
  ALDL_DEV_VY_BCM        = 0xF1,
  ALDL_DEV_VY_CLUSTER    = 0xF2,
  ALDL_DEV_VY_PIM        = 0xF3,
  ALDL_DEV_VY_PCM        = 0xF7,
  ALDL_DEV_VY_ABS        = 0xF9
};

#endif
