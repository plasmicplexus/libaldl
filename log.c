#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "log.h"

/* TODO: revisit whether a mutex is necessary */
static osal_mutex_t loglock;

void ctos(char *buf, void *dat, size_t len) {
  buf[0] = 0;

  char *c = buf;
  char *d = dat;

  while(len--)
    c += sprintf(c, "%02hhX%s", *(d++), len ? " " : "");
}

void pkttos(char *s, aldl_packet_t *pkt) {
  char buf[173];

  buf[0] = pkt->id;
  buf[1] = pkt->len + 0x55;
  memcpy(&buf[2], pkt->data, pkt->len);
  buf[pkt->len + 2] = pkt->chksum;

  ctos(s, buf, pkt->len + 3);
}

int logw(aldl_session_t *s, int level, char *sect, char *fmt, ...) {
  char l;
  va_list ap;

  osal_mutex_lock(&loglock);

  va_start(ap, fmt);

  if(level < s->conf.log_level || !s->conf.log_level)
    return 0;

  if(level > ALDL_LOGE || level < 0)
    return ALDL_EINVAL;

  switch(level) {
    case ALDL_LOGV: l = 'V'; break;
    case ALDL_LOGI: l = 'I'; break;
    case ALDL_LOGW: l = 'W'; break;
    case ALDL_LOGE: l = 'E'; break;
  }

  printf("%c [%s] ", l, sect);
  vprintf(fmt, ap);
  printf("\n");
  fflush(stdout);

  va_end(ap);

  osal_mutex_unlock(&loglock);

  return 0;
}

int log_init(void) {
  int ret;

  setvbuf(stdout, NULL, _IONBF, 0);

  ret = osal_mutex_create(&loglock);
  assert(ret, ALDL_ETHREAD);

  return 0;
}
