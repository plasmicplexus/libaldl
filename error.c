#include <stdio.h>

#include "aldl.h"
#include "error.h"

#define NERRS (sizeof(errors) / sizeof(struct errstr))

struct errstr {
  int num;
  char *s;
};

static const struct errstr errors[] = {
  { .num = ALDL_ESUCCESS,     .s = "Success"                                                 },
  { .num = ALDL_EINVAL,       .s = "Invalid argument"                                        },
  { .num = ALDL_EINIT,        .s = "Library not initialized"                                 },
  { .num = ALDL_ENOPORT,      .s = "Serial port not connected"                               },
  { .num = ALDL_EIO,          .s = "Input/output error"                                      },
  { .num = ALDL_ENORESP,      .s = "No response received"                                    },
  { .num = ALDL_EBADRESP,     .s = "Invalid response received"                               },
  { .num = ALDL_ENOMEM,       .s = "Cannot allocate memory"                                  },
  { .num = ALDL_ENOSYS,       .s = "Function not implemented"                                },
  { .num = ALDL_ETHREAD,      .s = "Threading error"                                         },
  { .num = ALDL_ETTYCONF,     .s = "Serial port configuration failed"                        },
  { .num = ALDL_ETTYOPEN,     .s = "Serial port open failed"                                 },
  { .num = ALDL_EPCAPOPEN,    .s = "PCAP file open failed"                                   },
  { .num = ALDL_EBADVEHICLE,  .s = "Requested function is not available for current vehicle" },
  { .num = ALDL_ENODRIVER,    .s = "Requested vehicle is unsupported"                        },
  { .num = ALDL_EMISSINGFEAT, .s = "Optional compile-time feature was not included"          }
};

APIEXPORT const char *aldl_strerror(int num) {
  int i;

  for(i = 0; i < NERRS; i++) {
    if(errors[i].num == num)
      return errors[i].s;
  }

  return "Unknown error";
}
