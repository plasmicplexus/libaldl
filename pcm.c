#include <string.h>

#include "aldl.h"

/*
 * APIEXPORT int aldl_pcm_ram_block(aldl_session_t *s, uint16_t addr, void *buf) {
 *   int ret;
 *
 *   aldl_packet_t pkt;
 *
 *   if(aldl_make(s->conf.vehicleid) != ALDL_MAKE_HOLDEN)
 *     return ALDL_EBADVEHICLE;
 *
 *   switch(aldl_model(s->conf.vehicleid)) {
 *     case ALDL_MODEL_COMMODORE_VT:
 *     case ALDL_MODEL_COMMODORE_VX:
 *     case ALDL_MODEL_COMMODORE_VY:
 *       break;
 *     default:
 *       return ALDL_EBADVEHICLE;
 *   }
 *
 *   char req[] = {
 *     0x02,
 *     htobe16(addr) >> 8,
 *     htobe16(addr)
 *   };
 *
 *   ret = aldl_send_wait(s, 0xF7, 4, req, sizeof(req), &pkt);
 *   if(ret < 0)
 *     return ALDL_EIO;
 *
 *   if(pkt.len != 65)
 *     return ALDL_EIO;
 *
 *   memcpy(buf, pkt.data + 1, 64);
 *   return 0;
 * }
 */
