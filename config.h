/* default amount of time to wait for responses (us) */
#define CONF_RX_TIMEOUT 150000

/* 
 * default timeout before rxloop flushes the input buffer (us)
 * a value of 0 disables input buffer flushing
 */
#define CONF_RX_BUF_TIMEOUT 0

/* default number of TX retries when waiting for a response */
#define CONF_TX_RETRIES 30

/* BCM silence command transmission interval (us) */
#define CONF_SILENCE_INTERVAL 3000000

/* whether or not to ignore echo by default */
#define CONF_IGNORE_ECHO 0

/* default baud rate to provide to the TTY driver */
#define CONF_BAUD 8192

/* optional library subsystems to include during compilation */
#define CONF_INCLUDE_GMSEEDKEY