#ifndef _ALDL_H
#define _ALDL_H

#include <stddef.h>
#include <stdint.h>

typedef int64_t aldl_time_t;

#include "osal/osal.h"

#define assert(x, y) { if((x)) { return y; }}

#define aldl_cvid(make, model) \
  (((make & 0xFFF) << 20) | ((model & 0xFFF) << 8))

#define aldl_cvid_module(cvid, module) \
  ((cvid & 0xFFFFFF00) | (module & 0xFF))

#define aldl_cvid_vehicleonly(cvid) \
  (cvid & 0xFFFFFF00)

#define aldl_make(cvid) \
  ((cvid >> 20) & 0xFFF)

#define aldl_model(cvid) \
  ((cvid >> 8) & 0xFFF)

#define aldl_module(cvid) \
  (cvid & 0xFF)

#define ALDL_LISTENER_RX 1
#define ALDL_LISTENER_TX 2

enum ALDL_DATATYPE {
  ALDL_BOOL   = 1,
  ALDL_INT    = 2,
  ALDL_FLOAT  = 3,
  ALDL_INTARR = 4,
  ALDL_STRING = 5,
};

enum ALDL_MAKE {
  ALDL_MAKE_HOLDEN    = 1,
  ALDL_MAKE_CHEVROLET = 2,
  ALDL_MAKE_PONTIAC   = 3,
  ALDL_MAKE_VAUXHALL  = 4
};

enum ALDL_MODULE {
  ALDL_MODULE_PIM        = 1,
  ALDL_MODULE_PCM        = 2,
  ALDL_MODULE_ECM        = 3,
  ALDL_MODULE_TCM        = 4,
  ALDL_MODULE_SRS        = 5,
  ALDL_MODULE_ABS        = 6,
  ALDL_MODULE_BCM        = 7,
  ALDL_MODULE_CLUSTER    = 8,
  ALDL_MODULE_HVAC       = 9,
  ALDL_MODULE_RADIO      = 10,
  ALDL_MODULE_TELEMATICS = 11
};

typedef uint32_t aldl_cvid_t;

typedef union {
  int      d_bool;
  int32_t  d_int;
  float    d_float;
  int     *d_intarr;
  char    *d_str;
} aldl_data_t;

typedef struct {
  int         id;
  int         len;
  uint8_t     chksum;
  char        data[170];
  aldl_time_t timestamp;
} aldl_packet_t;

typedef struct {
  volatile aldl_cvid_t    vehicle;
  volatile int            baud;
  volatile int            ignecho;
  volatile int            log_level;
  volatile int            log_rx;
  volatile int            log_tx;
  volatile aldl_time_t    rx_buf_timeout;
  volatile aldl_time_t    rx_timeout;
  volatile int            tx_retries;
  volatile aldl_time_t    tx_min_silence;
} aldl_sessionconf_t;

typedef void (*aldl_listener_t) (const aldl_packet_t*);

typedef struct {
  volatile int          running;
  osal_fd_t             tty;
  osal_fd_t             pcap;
  struct driver        *driver;
  aldl_sessionconf_t    conf;
  osal_thread_t         rxloop;
  osal_thread_t         txloop;
  struct listener      *listeners;
  struct waitpkt       *waiting;
  osal_mutex_t          wlock;
  struct txsched_entry *txsched;
  osal_mutex_t          txlock;
  volatile aldl_time_t  lastrx;
} aldl_session_t;

APIEXPORT int aldl_send(aldl_session_t*, int, int, void*, size_t);
APIEXPORT int aldl_wait_packet(aldl_session_t*, int, aldl_packet_t*, aldl_time_t);
APIEXPORT int aldl_send_wait(aldl_session_t*, int, int, void*, size_t, aldl_packet_t*);

APIEXPORT int aldl_register_txsched(aldl_session_t*, aldl_time_t, aldl_packet_t*, aldl_packet_t*, osal_mutex_t*);
APIEXPORT int aldl_unregister_txsched(aldl_session_t*, aldl_packet_t*);

APIEXPORT int aldl_session_end(aldl_session_t*);

#include "config.h"
#include "driver.h"
#include "error.h"

#endif
