#ifndef _MODCONF_H
#define _MODCONF_H

typedef struct aldl_conf_req {
  int                   error;
  int                   param;
  int                   module;
  aldl_data_t           val;
  struct aldl_conf_req *next;
} aldl_conf_req_t;

#endif
