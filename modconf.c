#include "aldl.h"
#include "modconf.h"

APIEXPORT int aldl_modconf_read(aldl_session_t *s, aldl_conf_req_t *req) {
  if(!s->driver)
    return ALDL_ENODRIVER;

  if(!s->driver->modconf_read)
    return ALDL_EBADVEHICLE;

  return s->driver->modconf_read(s, req);
}

APIEXPORT int aldl_modconf_write(aldl_session_t *s, aldl_conf_req_t *req) {
  if(!s->driver)
    return ALDL_ENODRIVER;

  if(!s->driver->modconf_read)
    return ALDL_EBADVEHICLE;

  return s->driver->modconf_write(s, req);
}
