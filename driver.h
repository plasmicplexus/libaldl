#ifndef _DRIVER_H
#define _DRIVER_H

#include <stdarg.h>
#include <stdint.h>

#include "aldl.h"
#include "dtc.h"
#include "modconf.h"
#include "vehicle.h"

struct driver {
  int (*modconf_read)    (aldl_session_t *s, aldl_conf_req_t *req);
  int (*modconf_write)   (aldl_session_t *s, aldl_conf_req_t *req);
  int (*cluster_text)    (aldl_session_t *s, char *str);
  int (*dtc_read)        (aldl_session_t *s, aldl_cvid_t module, aldl_dtc_t **dtclist, size_t *ndtclist);
  int (*dtc_clear)       (aldl_session_t *s, aldl_cvid_t module);
  int (*send_radstat)    (aldl_session_t *s, int stat, va_list ap);
  int (*silence_chatter) (aldl_session_t *s, int en);
  aldl_model_t models[];
};

extern struct driver *drivers[];
extern const size_t ndriver;

struct driver *getdriver(aldl_cvid_t);

#endif
