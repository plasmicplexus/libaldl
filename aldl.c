#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "aldl.h"
#include "log.h"
#include "pcap.h"

struct listener {
  int              type;
  aldl_listener_t  cb;
  struct listener *next;
};

struct waitpkt {
  int             rdy;
  osal_thread_t   tid;
  aldl_packet_t  *pkt;
  struct waitpkt *next;
};

struct txsched_entry {
  osal_mutex_t          mutex;
  aldl_packet_t        *pkt;
  aldl_packet_t        *match;
  aldl_time_t           interval;
  volatile int          last_succ;
  struct txsched_entry *next;
};

APIEXPORT int aldl_send(aldl_session_t *s, int id, int mode, void *data, size_t len) {
  int i;
  uint32_t c = 0;

  char buf[174];
  ssize_t out;

  char logbuf[2048];

  aldl_time_t us, lastrx;

  aldl_packet_t pkt;

  struct listener *l = s->listeners;

  assert(id < 0 || id > 0xFF, ALDL_EINVAL);
  assert(mode > 0xFF, ALDL_EINVAL);
  assert(len > (mode < 0 ? 170 : 169), ALDL_EINVAL);
  assert(!s->running, ALDL_ENOPORT);

  if(!data)
    len = 0;

  buf[0] = id;
  buf[1] = len + (mode < 0 ? 0x55 : 0x56);
  buf[2] = mode;
  memcpy(buf + (mode < 0 ? 2 : 3), data, len);

  pkt.id      = id;
  pkt.len     = len + (mode < 0 ? 0 : 1);
  pkt.data[0] = mode;
  memcpy(pkt.data, buf, len + (mode < 0 ? 0 : 1));

  len += mode < 0 ? 3 : 4;

  for(i = 0; i < len - 1; i++)
    c += buf[i];
  pkt.chksum = ~(c & 0xFF) + 1;
  buf[len - 1] = pkt.chksum;

  if(s->conf.tx_min_silence) {
    while((us = osal_micros()) - (lastrx = s->lastrx) < s->conf.tx_min_silence)
      osal_usleep(s->conf.tx_min_silence - (us - lastrx));
  }

  out = osal_tty_write(s->tty, buf, len);

  pkt.timestamp = osal_micros();

  if(s->conf.ignecho)
    osal_tty_flushin(s->tty);

  assert(out < len, ALDL_EIO);

  pcap_write(s, buf, len, 0);

  if(s->conf.log_tx) {
    ctos(logbuf, buf, len);
    logw(s, ALDL_LOGV, "tx", "%s", logbuf);
  }

  while(l) {
    if((l->type & ALDL_LISTENER_TX))
      l->cb(&pkt);
    l = l->next;
  }

  return 0;
}

APIEXPORT int aldl_wait_packet(aldl_session_t *s, int id, aldl_packet_t *pkt, aldl_time_t timeout) {
  int ret = 0;

  aldl_packet_t tmppkt;

  aldl_packet_t *p = pkt ? pkt : &tmppkt;

  struct waitpkt **wp;

  struct waitpkt w = {
    .tid = osal_thread_self(),
    .rdy = 0,
    .pkt = p
  };

  assert(id < 0 || id > 0xFF, ALDL_EINVAL);
  assert(!s->running, ALDL_ENOPORT);

  p->id = id;

  osal_mutex_lock(&s->wlock);
  w.next = s->waiting;
  s->waiting = &w;
  osal_mutex_unlock(&s->wlock);

  /* TODO: make an ALDL_INFINITE macro */
  if(timeout) {
    osal_usleep(timeout);
  } else {
    osal_pause();
  }

  osal_mutex_lock(&s->wlock);
  wp = &s->waiting;
  while(*wp) {
    if(*wp == &w) {
      *wp = (*wp)->next;
      continue;
    }

    wp = &(*wp)->next;
  }
  osal_mutex_unlock(&s->wlock);

  return w.rdy ? 0 : (timeout ? ALDL_ENORESP : -1);
}

APIEXPORT int aldl_send_wait(aldl_session_t *s, int id, int mode, void *data, size_t len, aldl_packet_t *pkt) {
  int i;
  int ret;

  aldl_packet_t tmppkt;

  aldl_packet_t *p = pkt ? pkt : &tmppkt;

  struct waitpkt **wp;

  struct waitpkt w = {
    .tid = osal_thread_self(),
    .rdy = 0,
    .pkt = p
  };

  assert(!id, ALDL_EINVAL);
  assert(mode > 0xFF, ALDL_EINVAL);
  assert(len > (mode < 0 ? 170 : 169), ALDL_EINVAL);
  assert(!s->running, ALDL_ENOPORT);

  if(!data)
    len = 0;

  p->id = id;

  osal_mutex_lock(&s->wlock);
  w.next = s->waiting;
  s->waiting = &w;
  osal_mutex_unlock(&s->wlock);

  for(i = 0; i < s->conf.tx_retries; i++) {
    ret = aldl_send(s, id, mode, data, len);
    if(ret)
      break;

    osal_usleep(s->conf.rx_timeout);

    if(w.rdy)
      break;
  }

  osal_mutex_lock(&s->wlock);
  wp = &s->waiting;
  while(*wp) {
    if(*wp == &w) {
      *wp = (*wp)->next;
      continue;
    }
    wp = &(*wp)->next;
  }
  osal_mutex_unlock(&s->wlock);

  if(ret)
    return ret;

  return w.rdy ? 0 : ALDL_ENORESP;
}

APIEXPORT int aldl_register_listener(aldl_session_t *s, aldl_listener_t cb, int type) {
  struct listener *l = s->listeners;

  if((type & ~3) || !type)
    return ALDL_EINVAL;

  while(l) {
    if(l->cb == cb)
      return 0;
    l = l->next;
  }

  l = malloc(sizeof(struct listener));
  if(!l)
    return ALDL_ENOMEM;

  l->cb = cb;
  l->type = type;
  l->next = s->listeners;
  s->listeners = l;

  return 0;
}

APIEXPORT int aldl_unregister_listener(aldl_session_t *s, aldl_listener_t cb) {
  int ret = ALDL_EINVAL;

  struct listener **l = &s->listeners;
  void *tmp;

  while(*l) {
    if((*l)->cb == cb) {
      tmp = *l;
      *l = (*l)->next;
      free(tmp);
      ret = 0;
      continue;
    }
    l = &(*l)->next;
  }

  return ret;
}

APIEXPORT int aldl_register_txsched(aldl_session_t *s, aldl_time_t interval, aldl_packet_t *pkt, aldl_packet_t *match, osal_mutex_t *mutex) {
  int ret;

  struct txsched_entry *te;
  struct txsched_entry *p;

  if(match && match->len > 170)
    return ALDL_EINVAL;

  p = s->txsched;
  while(p) {
    if(p->pkt == pkt) {
      *mutex = p->mutex;
      return 0;
    }
    p = p->next;
  }

  te = malloc(sizeof(struct txsched_entry));
  if(!te)
    return ALDL_ENOMEM;

  ret = osal_mutex_create(&te->mutex);
  if(ret) {
    free(te);
    return ALDL_ENOMEM;
  }

  te->pkt       = pkt;
  te->match     = match;
  te->interval  = interval;
  te->last_succ = 0;

  osal_mutex_lock(&s->txlock);
  te->next = s->txsched;
  s->txsched = te;
  osal_mutex_unlock(&s->txlock);

  *mutex = te->mutex;
  return 0;
}

APIEXPORT int aldl_unregister_txsched(aldl_session_t *s, aldl_packet_t *pkt) {
  int ret = ALDL_EINVAL;

  struct txsched_entry **p = &s->txsched;
  void *tmp;

  while(*p) {
    if((*p)->pkt == pkt) {
      tmp = *p;
      *p = (*p)->next;
      free(tmp);
      ret = 0;
      continue;
    }
    p = &(*p)->next;
  }

  return ret;
}

static void *rxloop(aldl_session_t *s) {
  int i, j;
  int ret;
  uint8_t chksum;

  ssize_t in;
  int rxn, rxpos;
  static uint8_t rxbuf[173];
  uint8_t *rxstart;
  aldl_packet_t pkt;

  aldl_time_t lastrx, us;

  struct txsched_entry *te;
  struct listener *l;
  struct waitpkt *w;

  char logbuf[2048];

  /* try to get realtime scheduler priority */
  ret = osal_get_realtime();
  if(ret)
    logw(s, ALDL_LOGW, "rx", "Failed to set scheduler priority");

  us = osal_micros();
  rxn = 0;
  rxpos = 0;
  while(s->running) {
    in = osal_tty_read(s->tty, rxbuf + rxpos, 1);

    lastrx = us;
    us = osal_micros();

    s->lastrx = us;

    /* TODO: error handling */
    if(in < 1)
      continue;

    /* purge the buffer if timeout between bytes exceeded */
    if(s->conf.rx_buf_timeout && us - lastrx > s->conf.rx_buf_timeout) {
      rxn = 0;
      rxpos = 0;
      continue;
    }

    if(rxn < sizeof(rxbuf))
      rxn++;

    rxpos = (rxpos + 1) % sizeof(rxbuf);

    if(rxn < 3)
      continue;

    for(i = 0; i < rxn; i++) {
      /* search the buffer for a valid packet */
      pkt.id        = rxbuf[(rxpos - rxn + i) % sizeof(rxbuf)];
      pkt.len       = rxbuf[(rxpos - rxn + i + 1) % sizeof(rxbuf)];
      pkt.timestamp = us;
      if(!pkt.id)
        continue;
      if(pkt.len < 0x55)
        continue;
      pkt.len -= 0x55;
      if(pkt.len > rxn - i - 3)
        continue;

      pkt.chksum = rxbuf[(rxpos - rxn + i + pkt.len + 2) % sizeof(rxbuf)];

      /* calculate and verify checksum*/
      chksum = 0;
      for(j = 0; j < pkt.len + 3; j++) {
        chksum += rxbuf[(rxpos - rxn + i + j) % sizeof(rxbuf)];
        if(j > 1)
          pkt.data[j - 2] = rxbuf[(rxpos - rxn + i + j) % sizeof(rxbuf)];
      }
      if(chksum)
        break;
      else
        goto chksum_valid;
    }

    continue;
  chksum_valid:

    rxn = 0;
    rxpos = 0;

    pcap_write(s, rxbuf, pkt.len + 3, us);

    if(s->conf.log_rx) {
      pkttos(logbuf, &pkt);
      logw(s, ALDL_LOGV, "rx", "%s", logbuf);
    }

    osal_mutex_lock(&s->txlock);
    te = s->txsched;
    while(te) {
      if(te->match && te->match->len == pkt.len) {
        if(!memcmp(te->match->data, pkt.data, pkt.len))
          te->last_succ = 1;
      }
      te = te->next;
    }
    osal_mutex_unlock(&s->txlock);

    osal_mutex_lock(&s->wlock);
    w = s->waiting;
    while(w) {
      if(!w->rdy && (w->pkt->id == pkt.id || !w->pkt->id)) {
        memcpy(w->pkt, &pkt, sizeof(pkt));
        w->rdy = 1;
        osal_thread_wake(w->tid);
        osal_yield();
      }
      w = w->next;
    }
    osal_mutex_unlock(&s->wlock);

    l = s->listeners;
    while(l) {
      if((l->type & ALDL_LISTENER_RX))
        l->cb(&pkt);
      l = l->next;
    }

    memset(rxbuf, 0, pkt.len + 3);
  }

  return NULL;
}

static void *txloop(aldl_session_t *s) {
  aldl_time_t next;
  aldl_time_t sleep;

  struct txsched_entry *p;

  char logbuf[2048];

  while(s->running) {
    sleep = 0;

    p = s->txsched;
    while(p) {
      next = !p->last_succ && p->match ? s->conf.rx_timeout : p->interval;

      if(next < osal_micros()) {
        osal_mutex_lock(&p->mutex);
        if(s->conf.log_tx) {
          ctos(logbuf, p->pkt->data, p->pkt->len);
          logw(s, ALDL_LOGV, "tx", " L%s %s", p->last_succ ? "SUCC" : "FAIL", logbuf);
        }
        aldl_send(s, p->pkt->id, -1, p->pkt->data, p->pkt->len);
        p->last_succ = 0;
        osal_mutex_unlock(&p->mutex);
      }

      if(next < sleep || !sleep)
        sleep = next;

      p = p->next;
    }

    osal_usleep(sleep ? sleep : 1000000);
  }

  return NULL;
}

int aldl_silence_chatter(aldl_session_t *s, int en) {
  assert(!s->driver, ALDL_ENODRIVER);
  assert(!s->driver->silence_chatter, ALDL_EBADVEHICLE);

  return s->driver->silence_chatter(s, en);
}

APIEXPORT int aldl_session_openport(aldl_session_t *s, const char *path) {
  int ret;

  /*
   * TODO: prevent race conditions on
   *  - s->running
   *  - s->rxloop
   *  - s->txloop
   */
  if(s->running)
    return ALDL_EINVAL;

  ret = osal_tty_open(&s->tty, path);
  if(ret) {
    ret = ALDL_ETTYOPEN;
    goto error;
  }

  ret = osal_tty_conf(s->tty, s->conf.baud);
  if(ret) {
    ret = ALDL_ETTYCONF;
    goto error;
  }

  s->running = 1;

  ret = osal_thread_create(&s->rxloop, &rxloop, s);
  if(ret) goto error;
  ret = osal_thread_create(&s->txloop, &txloop, s);
  if(ret) goto error;

error:
  osal_tty_close(s->tty);
  return ret;
}

APIEXPORT int aldl_session_closeport(aldl_session_t *s) {
  s->running = 0;
  osal_thread_join(s->rxloop);
  osal_thread_join(s->txloop);
  osal_tty_close(s->tty);
  s->rxloop = 0;
  s->txloop = 0;
  return 0;
}

APIEXPORT int aldl_session_getdefaultconf(aldl_sessionconf_t *c) {
  memset(c, 0, sizeof(aldl_sessionconf_t));
  c->baud           = CONF_BAUD;
  c->ignecho        = CONF_IGNORE_ECHO;
  c->log_level      = ALDL_LOG_NONE;
  c->rx_buf_timeout = CONF_RX_BUF_TIMEOUT;
  c->rx_timeout     = CONF_RX_TIMEOUT;
  c->tx_retries     = CONF_TX_RETRIES;
  return 0;
}

APIEXPORT int aldl_session_getconf(aldl_session_t *s, aldl_sessionconf_t *c) {
  memcpy(c, &s->conf, sizeof(aldl_sessionconf_t));
  return 0;
}

APIEXPORT int aldl_session_setconf(aldl_session_t *s, aldl_sessionconf_t *c) {
  int ret;

  assert(aldl_module(c->vehicle), ALDL_EINVAL);

  if(s->conf.baud != c->baud) {
    ret = osal_tty_conf(s->tty, c->baud);
    assert(ret, ALDL_ETTYCONF);
  }

  s->conf.vehicle        = c->vehicle;
  s->conf.baud           = c->baud;
  s->conf.ignecho        = c->ignecho;
  s->conf.log_level      = c->log_level;
  s->conf.log_rx         = c->log_rx;
  s->conf.log_tx         = c->log_tx;
  s->conf.rx_buf_timeout = c->rx_buf_timeout;
  s->conf.rx_timeout     = c->rx_timeout;
  s->conf.tx_retries     = c->tx_retries;
  s->conf.tx_min_silence = c->tx_min_silence;
  s->driver = getdriver(s->conf.vehicle);

  return 0;
}

APIEXPORT int aldl_session_create(aldl_session_t **session) {
  int ret;

  aldl_session_t *s = malloc(sizeof(aldl_session_t));
  assert(!s, ALDL_ENOMEM);
  memset(s, 0, sizeof(aldl_session_t));

  aldl_session_getdefaultconf(&s->conf);

  ret = osal_mutex_create(&s->wlock);
  if(ret) goto error;
  ret = osal_mutex_create(&s->txlock);
  if(ret) goto error;

  *session = s;
  return 0;

error:
  aldl_session_end(s);
  return ret;
}

APIEXPORT int aldl_session_end(aldl_session_t *s) {
  struct listener      *lp = s->listeners;
  struct txsched_entry *tp = s->txsched;
  struct waitpkt       *wp = s->waiting;

  void *tmp;

  aldl_session_closeport(s);
  osal_mutex_destroy(s->wlock);
  osal_mutex_destroy(s->txlock);
  osal_pcap_close(s->pcap);

  /* free listeners */
  while(lp) {
    tmp = lp;
    lp = lp->next;
    free(tmp);
  }

  /* free txsched entries */
  while(tp) {
    tmp = tp;
    tp = tp->next;
    free(tmp);
  }

  /* free waiting packets */
  while(wp) {
    tmp = wp;
    wp = wp->next;
    free(tmp);
  }

  free(s);
  return 0;
}

APIEXPORT int aldl_init(void) {
  int ret;

  ret = log_init();
  assert(ret, ret);

  return 0;
}
