#include <stdint.h>

#include "aldl.h"
#include "driver.h"

extern struct driver driver_holden;

struct driver *drivers[] = {
  &driver_holden
};

const size_t ndriver = sizeof(drivers) / sizeof(struct driver*);

struct driver *getdriver(aldl_cvid_t cvid) {
  int i;

  aldl_model_t *m;

  assert(aldl_module(cvid), NULL);

  for(i = 0; i < ndriver; i++) {
    m = drivers[i]->models;
    while(m->vehicle) {
      if(m->vehicle == cvid)
        return drivers[i];
      m += sizeof(aldl_model_t);
    }
  }

  return NULL;
}
