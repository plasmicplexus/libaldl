#ifndef _GMSEEDKEY_H
#define _GMSEEDKEY_H

#include <stdint.h>

#include "osal/osal.h"

#define GM_ALG_CLUSTER 0x200

APIEXPORT int gmseedkey(int, uint16_t, uint16_t*);

#endif
