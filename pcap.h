#ifndef _PCAP_H
#define _PCAP_H

#include "aldl.h"

void pcap_write(aldl_session_t*, void*, size_t, aldl_time_t);

#endif