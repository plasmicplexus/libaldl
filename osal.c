#if defined(_WIN32)
#include "osal/windows.c"
#elif defined(__linux__)
#include "osal/linux.c"
#elif defined(__APPLE__)
#include "osal/osx.c"
#else
#error Compiling for unknown target!
#endif