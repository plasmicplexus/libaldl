#include <Windows.h>

#pragma comment(lib, "Ws2_32.lib")

#define APIEXPORT __declspec(dllexport)

#define be16toh(x) (ntohs(x))
#define be32toh(x) (ntohl(x))
#define htobe16(x) (htons(x))
#define htobe32(x) (htonl(x))

typedef long int ssize_t;

typedef HANDLE osal_fd_t;

typedef HANDLE osal_mutex_t;

typedef HANDLE osal_thread_t;
