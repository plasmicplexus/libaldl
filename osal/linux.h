#include <endian.h>
#include <pthread.h>
#include <unistd.h>

#define APIEXPORT __attribute__((visibility("default")))

typedef int osal_fd_t;

typedef pthread_mutex_t osal_mutex_t;

typedef pthread_t osal_thread_t;
