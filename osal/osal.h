#ifndef _OSAL_OSAL_H
#define _OSAL_OSAL_H

#include "../aldl.h"

#if defined(_WIN32)
#include "windows.h"
#elif defined(__linux__)
#include "linux.h"
#elif defined(__APPLE__)
#include "osx.h"
#else
#error Compiling for unknown target!
#endif

/* TIME */
aldl_time_t osal_micros(void);
void osal_usleep(aldl_time_t);

/* TTY */
int osal_tty_open(osal_fd_t*, const char*);
int osal_tty_close(osal_fd_t);
int osal_tty_conf(osal_fd_t, int);
int osal_tty_flushin(osal_fd_t);
ssize_t osal_tty_read(osal_fd_t, void*, size_t);
ssize_t osal_tty_write(osal_fd_t, void*, size_t);

/* PCAP */
int osal_pcap_open(osal_fd_t*, const char*);
int osal_pcap_close(osal_fd_t);
ssize_t osal_pcap_write(osal_fd_t, void*, size_t);

/* THREADING */
int osal_get_realtime(void);

int osal_mutex_create(osal_mutex_t*);
int osal_mutex_destroy(osal_mutex_t); /* TODO: make this a pointer */
void osal_mutex_lock(osal_mutex_t*);
void osal_mutex_unlock(osal_mutex_t*);

osal_thread_t osal_thread_self(void);
int osal_thread_create(osal_thread_t*, void *(*)(void*), void*);
int osal_thread_wake(osal_thread_t);
int osal_thread_join(osal_thread_t);
void osal_yield(void);
void osal_pause(void);

#endif
