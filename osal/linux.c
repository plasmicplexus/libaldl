#define termios asmtermios
#include <asm/termbits.h>
#undef termios
#include <termios.h>

#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include "../aldl.h"
#include "../config.h"
#include "../error.h"
#include "linux.h"

aldl_time_t osal_micros(void) {
  struct timeval tv;

  gettimeofday(&tv, NULL);

  return (tv.tv_sec * 1000000) + tv.tv_usec;
}

void osal_usleep(aldl_time_t us) {
  struct timespec ts;
  ts.tv_sec  = us / 1000000;
  ts.tv_nsec = (us % 1000000) * 1000;
  while(nanosleep(&ts, &ts) < 0 && errno == EINTR);
}

int osal_tty_open(osal_fd_t *tty, const char *path) {
  int ret;

  struct termios2 tios;

  int fd = open(path, O_RDWR | O_NONBLOCK | O_NOCTTY);
  if(fd < 0)
    return ALDL_ETTYOPEN;

  ret = ioctl(fd, TCGETS2, &tios);
  if(ret < 0)
    goto conf_error;

  tios.c_cflag &= ~(PARENB | CSTOPB | CSIZE | CRTSCTS | CBAUD);
  tios.c_cflag |=   CS8 | CREAD | CLOCAL | BOTHER;
  tios.c_lflag &= ~(ICANON | ISIG | ECHO | ECHOE | ECHONL);
  tios.c_iflag &= ~(IXON | IXOFF | IXANY);
  tios.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);
  tios.c_oflag &= ~(OPOST | ONLCR);

  ret = ioctl(fd, TCSETS2, &tios);
  if(ret < 0)
    goto conf_error;

  *tty = fd;
  return 0;

conf_error:
  close(fd);
  return ALDL_ETTYCONF;
}

int osal_tty_close(osal_fd_t tty) {
  return close(tty) ? -1 : 0;
}

int osal_tty_conf(osal_fd_t tty, int baud) {
  int ret;

  struct termios2 tios;

  ret = ioctl(tty, TCGETS2, &tios);
  assert(ret < 0, ALDL_ETTYCONF);

  tios.c_ispeed = baud;
  tios.c_ospeed = baud;

  ret = ioctl(tty, TCSETS2, &tios);

  return ret < 0 ? ALDL_ETTYCONF : 0;
}

int osal_tty_flushin(osal_fd_t tty) {
  return tcflush(tty, TCIFLUSH) ? -1 : 0;
}

ssize_t osal_tty_read(osal_fd_t tty, void *buf, size_t len) {
  return read(tty, buf, len);
}

ssize_t osal_tty_write(osal_fd_t tty, void *buf, size_t len) {
  return write(tty, buf, len);
}

int osal_pcap_open(osal_fd_t *fd, const char *path) {
  *fd = open(path, O_RDWR | O_CREAT | O_TRUNC);
  if(*fd < 0)
    return ALDL_EPCAPOPEN;

  return 0;
}

int osal_pcap_close(osal_fd_t fd) {
  return close(fd) ? -1 : 0;
}

ssize_t osal_pcap_write(osal_fd_t fd, void *buf, size_t len) {
  return write(fd, buf, len);
}

int osal_get_realtime(void) {
  struct sched_param sched = {
    .sched_priority = sched_get_priority_max(SCHED_FIFO)
  };

  return sched_setscheduler(0, SCHED_FIFO, &sched) < 0 ? -1 : 0;
}

int osal_mutex_create(osal_mutex_t *mutex) {
  int ret = pthread_mutex_init(mutex, NULL);

  if(ret == ENOMEM)
    return ALDL_ENOMEM;

  return ret ? -1 : 0;
}

int osal_mutex_destroy(osal_mutex_t mutex) {
  return pthread_mutex_destroy(&mutex) ? -1 : 0;
}

void osal_mutex_lock(osal_mutex_t *mutex) {
  pthread_mutex_lock(mutex);
}

void osal_mutex_unlock(osal_mutex_t *mutex) {
  pthread_mutex_unlock(mutex);
}

osal_thread_t osal_thread_self(void) {
  return pthread_self();
}

int osal_thread_create(osal_thread_t *thread, void *(*entry) (void*), void *args) {
  return pthread_create(thread, NULL, entry, args) ? ALDL_ETHREAD : 0;
}

int osal_thread_wake(osal_thread_t thread) {
  return pthread_kill(thread, SIGCONT) ? ALDL_ETHREAD : 0;
}

int osal_thread_join(osal_thread_t thread) {
  return pthread_join(thread, NULL) ? ALDL_ETHREAD : 0;
}

void osal_yield(void) {
  sched_yield();
}

void osal_pause(void) {
  pause();
}
