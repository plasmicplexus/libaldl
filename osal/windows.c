#include <Windows.h>

#include "../aldl.h"
#include "../log.h"

aldl_time_t osal_micros(void) {
  ULARGE_INTEGER t;
  FILETIME time;

  GetSystemTimeAsFileTime(&time);
  t.LowPart = time.dwLowDateTime;
  t.HighPart = time.dwHighDateTime;

  return ((t.QuadPart / 10) - 11644473600000000);
}

void osal_usleep(aldl_time_t delay) {
  BOOL ret;

  LARGE_INTEGER dt;
  dt.QuadPart = delay * -10;

  HANDLE timer = CreateWaitableTimer(NULL, TRUE, NULL);
  if(!timer)
    goto imprecise_sleep;

  ret = SetWaitableTimer(timer, &dt, 0, NULL, NULL, FALSE);
  if(!ret)
    goto imprecise_sleep;
  WaitForSingleObject(timer, INFINITE);
  CloseHandle(timer);
  return;

imprecise_sleep:
  Sleep(delay / 1000);
}

int osal_tty_open(osal_fd_t *tty, const char *path) {
  HANDLE h = CreateFileA(
    path,
    GENERIC_READ | GENERIC_WRITE,
    FILE_SHARE_READ,
    NULL,
    OPEN_EXISTING,
    FILE_FLAG_OVERLAPPED | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING,
    NULL);

  if(h == INVALID_HANDLE_VALUE)
    return -1;

  *tty = h;
  return 0;
}

int osal_tty_close(osal_fd_t tty) {
  FlushFileBuffers(tty);
  return CloseHandle(tty) ? 0 : -1;
}

int osal_tty_conf(osal_fd_t tty, int baud) {
  int ret;

  DCB dcb;
  COMMTIMEOUTS cto;

  ret = GetCommState(tty, &dcb);
  assert(!ret, ALDL_ETTYCONF);
  dcb.BaudRate = baud;
  dcb.ByteSize = 8;
  dcb.StopBits = ONESTOPBIT;
  dcb.Parity = NOPARITY;
  ret = SetCommState(tty, &dcb);
  assert(!ret, ALDL_ETTYCONF);

  ret = SetupComm(tty, 4096, 4096);
  assert(!ret, ALDL_ETTYCONF);

  ret = GetCommTimeouts(tty, &cto);
  assert(!ret, ALDL_ETTYCONF);
  cto.ReadIntervalTimeout = -1;
  cto.ReadTotalTimeoutMultiplier = -1;
  cto.ReadTotalTimeoutConstant = -2;
  ret = SetCommTimeouts(tty, &cto);
  assert(!ret, ALDL_ETTYCONF);

  return 0;
}

int osal_tty_flushin(osal_fd_t tty) {
  return PurgeComm(tty, PURGE_RXCLEAR) ? 0 : -1;
}

ssize_t osal_tty_read(osal_fd_t tty, void *buf, size_t len) {
  BOOL ret;
  DWORD n;

  OVERLAPPED overlapped;
  memset(&overlapped, 0, sizeof(overlapped));

  ReadFile(tty, buf, len, NULL, &overlapped);
  ret = GetOverlappedResult(tty, &overlapped, &n, TRUE);

  return ret ? n : -1;
}

ssize_t osal_tty_write(osal_fd_t tty, void *buf, size_t len) {
  BOOL ret;
  DWORD n;
  DWORD nn;

  OVERLAPPED overlapped;
  memset(&overlapped, 0, sizeof(overlapped));

  int wret = WriteFile(tty, buf, len, &nn, &overlapped);
  int werr = GetLastError();
  while(GetLastError() == ERROR_IO_PENDING)
    ret = GetOverlappedResult(tty, &overlapped, &n, TRUE);
  int oerr = GetLastError();

  if(!ret || n < len) {
    int x = 1;
  }

  return ret ? n : -1;
}

int osal_pcap_open(osal_fd_t *pcap, const char *path) {
  HANDLE h = CreateFileA(
    path,
    GENERIC_READ | GENERIC_WRITE,
    FILE_SHARE_READ,
    NULL,
    CREATE_ALWAYS,
    0,
    NULL);

  if(h == INVALID_HANDLE_VALUE)
    return -1;

  *pcap = h;
  return 0;
}

int osal_pcap_close(osal_fd_t pcap) {
  FlushFileBuffers(pcap);
  return CloseHandle(pcap) ? 0 : -1;
}

ssize_t osal_pcap_write(osal_fd_t pcap, void *buf, size_t len) {
  BOOL ret;
  DWORD n;

  ret = WriteFile(pcap, buf, len, &n, NULL);
  DWORD error = GetLastError();
  return ret ? n : -1;
}

int osal_get_realtime(void) {
  int ret;

  ret = SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
  if(!ret)
    return -1;

  ret = SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
  if(!ret)
    return -1;

  return 0;
}

int osal_mutex_create(osal_mutex_t *mutex) {
  *mutex = CreateMutex(NULL, FALSE, NULL);
  return *mutex == NULL ? -1 : 0;
}

int osal_mutex_destroy(osal_mutex_t mutex) {
  return CloseHandle(mutex) ? 0 : -1;
}

void osal_mutex_lock(osal_mutex_t *mutex) {
  WaitForSingleObject(*mutex, INFINITE);
}

void osal_mutex_unlock(osal_mutex_t *mutex) {
  ReleaseMutex(*mutex);
}

osal_thread_t osal_thread_self(void) {
  return GetCurrentThread();
}

int osal_thread_create(osal_thread_t *thread, void *(*entry) (void*), void *args) {
  *thread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) entry, args, 0, NULL);
  return !*thread ? ALDL_ETHREAD : 0;
}

int osal_thread_wake(osal_thread_t thread) {
  return ResumeThread(thread) == -1 ? ALDL_ETHREAD : 0;
}

int osal_thread_join(osal_thread_t thread) {
  return WaitForSingleObject(thread, INFINITE) ? 0 : -1;
}

void osal_yield(void) {
  SwitchToThread();
}

void osal_pause(void) {
  Sleep(INFINITE);
}