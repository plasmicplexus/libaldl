#include <machine/endian.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <unistd.h>

#define APIEXPORT __attribute__((visibility("default")))

#define be16toh(x) (ntohs(x))
#define be32toh(x) (ntohl(x))
#define htobe16(x) (htons(x))
#define htobe32(x) (htonl(x))

typedef int osal_fd_t;

typedef pthread_mutex_t osal_mutex_t;

typedef pthread_t osal_thread_t;
