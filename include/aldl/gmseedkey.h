#ifndef _ALDL_GMSEEDKEY_H
#define _ALDL_GMSEEDKEY_H

#include <stdint.h>

#define GM_ALG_CLUSTER 0x200

int gmseedkey(int algorithm, uint16_t seed, uint16_t *key);

#endif
