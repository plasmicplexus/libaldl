#ifndef _ALDL_MODCONF_H
#define _ALDL_MODCONF_H

#include <aldl/aldl.h>

typedef struct aldl_conf_req {
  int                   error;
  int                   param;
  int                   module;
  aldl_data_t           val;
  struct aldl_conf_req *next;
} aldl_conf_req_t;

int aldl_modconf_read(aldl_session_t session, aldl_conf_req_t *req);
int aldl_modconf_write(aldl_session_t session, aldl_conf_req_t *req);

#endif
