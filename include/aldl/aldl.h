#ifndef _ALDL_ALDL_H
#define _ALDL_ALDL_H

#include <stddef.h>
#include <stdint.h>

typedef int64_t aldl_time_t;

#include <aldl/osal/osal.h>

#define aldl_cvid(make, model) \
  (((make & 0xFFF) << 20) | ((model & 0xFFF) << 8))

#define aldl_cvid_module(cvid, module) \
  ((cvid & 0xFFFFFF00) | (module & 0xFF))

#define aldl_cvid_vehicleonly(cvid) \
  (cvid & 0xFFFFFF00)

#define aldl_make(cvid) \
  ((cvid >> 20) & 0xFFF)

#define aldl_model(cvid) \
  ((cvid >> 8) & 0xFFF)

#define aldl_module(cvid) \
  (cvid & 0xFF)

#define ALDL_LISTENER_RX 1
#define ALDL_LISTENER_TX 2

enum ALDL_DATATYPE {
  ALDL_BOOL   = 1,
  ALDL_INT    = 2,
  ALDL_FLOAT  = 3,
  ALDL_INTARR = 4,
  ALDL_STRING = 5,
};

enum ALDL_MAKE {
  ALDL_MAKE_HOLDEN    = 1,
  ALDL_MAKE_CHEVROLET = 2,
  ALDL_MAKE_PONTIAC   = 3,
  ALDL_MAKE_VAUXHALL  = 4
};

enum ALDL_MODULE {
  ALDL_MODULE_PIM        = 1,
  ALDL_MODULE_PCM        = 2,
  ALDL_MODULE_ECM        = 3,
  ALDL_MODULE_TCM        = 4,
  ALDL_MODULE_SRS        = 5,
  ALDL_MODULE_ABS        = 6,
  ALDL_MODULE_BCM        = 7,
  ALDL_MODULE_CLUSTER    = 8,
  ALDL_MODULE_HVAC       = 9,
  ALDL_MODULE_RADIO      = 10,
  ALDL_MODULE_TELEMATICS = 11
};

typedef uint32_t aldl_cvid_t;

typedef union {
  int      d_bool;
  int32_t  d_int;
  float    d_float;
  int     *d_intarr;
  char    *d_str;
} aldl_data_t;

typedef struct {
  int         id;
  int         len;
  uint8_t     chksum;
  char        data[170];
  aldl_time_t timestamp;
} aldl_packet_t;

typedef struct {
  aldl_cvid_t    vehicle;
  int            baud;
  int            ignecho;
  int            log_level;
  int            log_rx;
  int            log_tx;
  aldl_time_t    rx_buf_timeout;
  aldl_time_t    rx_timeout;
  int            tx_retries;
  aldl_time_t    tx_min_silence;
} aldl_sessionconf_t;

typedef void (*aldl_listener_t) (const aldl_packet_t *pkt);

typedef void *aldl_session_t;

int aldl_send(aldl_session_t session, int id, int mode, void *data, size_t len);
int aldl_wait_packet(aldl_session_t session, int id, aldl_packet_t *pkt, aldl_time_t timeout);
int aldl_send_wait(aldl_session_t session, int id, int mode, void *data, size_t len, aldl_packet_t *resp);

int aldl_register_listener(aldl_session_t session, aldl_listener_t listener, int type);
int aldl_unregister_listener(aldl_session_t session, aldl_listener_t listener);

int aldl_register_txsched(aldl_session_t session, aldl_time_t interval, aldl_packet_t *pkt, aldl_packet_t *resp_match, aldl_mutex_t *pkt_lock);
int aldl_unregister_txsched(aldl_session_t session, aldl_packet_t *pkt);

int aldl_silence_chatter(aldl_session_t session, int en_silence);

int aldl_session_openport(aldl_session_t session, const char *path);
int aldl_session_closeport(aldl_session_t session);
int aldl_session_getdefaultconf(aldl_sessionconf_t *conf);
int aldl_session_getconf(aldl_session_t session, aldl_sessionconf_t *conf);
int aldl_session_setconf(aldl_session_t session, aldl_sessionconf_t *conf);
int aldl_session_create(aldl_session_t *session);
int aldl_session_end(aldl_session_t session);

int aldl_init(void);

#include <aldl/cluster.h>
#include <aldl/dtc.h>
#include <aldl/error.h>
#include <aldl/gmseedkey.h>
#include <aldl/log.h>
#include <aldl/modconf.h>
#include <aldl/pcap.h>
#include <aldl/radio.h>
#include <aldl/vehicle.h>

#include <aldl/make/holden.h>

#endif
