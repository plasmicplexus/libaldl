#ifndef _ALDL_OSAL_LINUX_H
#define _ALDL_OSAL_LINUX_H

#include <endian.h>
#include <pthread.h>
#include <unistd.h>

#define APIIMPORT

typedef pthread_mutex_t aldl_mutex_t;

#endif
