#ifndef _ALDL_OSAL_WINDOWS_H
#define _ALDL_OSAL_WINDOWS_H

#include <Windows.h>

#define APIIMPORT __declspec(dllimport)

typedef long int ssize_t;

typedef HANDLE aldl_mutex_t;

#endif