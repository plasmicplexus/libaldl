#ifndef _ALDL_OSAL_OSAL_H
#define _ALDL_OSAL_OSAL_H

#if defined(_WIN32)
#include <aldl/osal/windows.h>
#elif defined(__linux__)
#include <aldl/osal/linux.h>
#elif defined(__APPLE__)
#include <aldl/osal/osx.h>
#else
#error Compiling for unknown target!
#endif

#endif