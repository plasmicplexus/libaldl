#ifndef _ALDL_OSAL_OSX_H
#define _ALDL_OSAL_OSX_H

#include <machine/endian.h>
#include <pthread.h>
#include <unistd.h>

#define APIIMPORT

typedef pthread_mutex_t aldl_mutex_t;

#endif
