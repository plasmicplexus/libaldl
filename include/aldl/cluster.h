#ifndef _ALDL_CLUSTER_H
#define _ALDL_CLUSTER_H

#include <stdint.h>

int aldl_cluster_text(aldl_session_t session, const char *text);

#endif
