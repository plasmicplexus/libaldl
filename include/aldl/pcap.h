#ifndef _ALDL_PCAP_H
#define _ALDL_PCAP_H

int aldl_pcap_start(aldl_session_t session, const char *pcap_path);
int aldl_pcap_stop(aldl_session_t session);

#endif