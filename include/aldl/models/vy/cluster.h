#ifndef _ALDL_MODELS_VY_CLUSTER_H
#define _ALDL_MODELS_VY_CLUSTER_H

#ifndef ALDL_PRNDL
#define ALDL_PRNDL

#define ALDL_PRNDL_P 7
#define ALDL_PRNDL_R 6
#define ALDL_PRNDL_N 5
#define ALDL_PRNDL_D 4
#define ALDL_PRNDL_3 3
#define ALDL_PRNDL_2 2
#define ALDL_PRNDL_1 1

#endif

#pragma pack(1)
typedef struct {
  uint16_t : 16;

  /* function output enable words */
  uint8_t en_btn : 1;
  uint8_t        : 7;

  uint8_t : 8;

  uint8_t en_warn_fuel_low    : 1;
  uint8_t en_warn_cel         : 1;
  uint8_t en_warn_temp        : 1;
  uint8_t                     : 1;
  uint8_t en_warn_oil         : 1;
  uint8_t en_warn_coolant_low : 1;
  uint8_t                     : 1;
  uint8_t                     : 1;

  uint8_t en_warn_seat_belt : 1;
  uint8_t en_warn_srs       : 1;
  uint8_t en_warn_brake     : 1;
  uint8_t en_warn_alt       : 1;
  uint8_t en_warn_fuel_vlow : 1;
  uint8_t                   : 1;
  uint8_t                   : 1;
  uint8_t en_warn_traction  : 1;

  uint8_t en_warn_park_lamp       : 1;
  uint8_t en_warn_brake_lamp      : 1;
  uint8_t en_warn_brake_lamp_fuse : 1;
  uint8_t en_warn_abs             : 1;
  uint8_t                         : 1;
  uint8_t en_power_shift          : 1;
  uint8_t en_warn_overspeed       : 1;
  uint8_t en_cruise_arm           : 1;

  uint8_t en_cruise_act : 1;
  uint8_t en_prndl      : 1;
  uint8_t               : 1;
  uint8_t               : 1;
  uint8_t               : 1;
  uint8_t               : 1;
  uint8_t               : 1;
  uint8_t               : 1;

  uint8_t en_tcs        : 1;
  uint8_t               : 1;
  uint8_t               : 1;
  uint8_t               : 1;
  uint8_t               : 1;
  uint8_t               : 1;
  uint8_t en_buzzer     : 1;
  uint8_t               : 1;

  /* gauge output enable word */
  uint8_t                 : 1;
  uint8_t en_temp         : 1;
  uint8_t en_fuel         : 1;
  uint8_t en_tacho        : 1;
  uint8_t en_speedo       : 1;
  uint8_t en_bright_lcd   : 1;
  uint8_t en_bright_ptr   : 1;
  uint8_t en_bright_gauge : 1;

  uint8_t                 : 5;
  uint8_t en_cluster_test : 1;
  uint8_t en_lcd_test     : 1;
  uint8_t                 : 1;

  uint16_t : 16;
  uint8_t  : 8;

  uint8_t btn_set       : 1;
  uint8_t btn_mode      : 1;
  uint8_t btn_up        : 1;
  uint8_t btn_down      : 1;
  uint8_t               : 4;

  uint64_t : 64;
  uint32_t : 32;
  uint16_t : 16;
  uint8_t  : 8;

  /* function output state words */
  uint8_t warn_fuel_low    : 1;
  uint8_t warn_cel         : 1;
  uint8_t warn_temp        : 1;
  uint8_t                  : 1;
  uint8_t warn_oil         : 1;
  uint8_t warn_coolant_low : 1;
  uint8_t                  : 1;
  uint8_t                  : 1;

  uint8_t warn_seat_belt : 1;
  uint8_t warn_srs       : 1;
  uint8_t warn_brake     : 1;
  uint8_t warn_alt       : 1;
  uint8_t warn_fuel_vlow : 1;
  uint8_t                : 1;
  uint8_t                : 1;
  uint8_t warn_traction  : 1;

  uint8_t warn_park_lamp       : 1;
  uint8_t warn_brake_lamp      : 1;
  uint8_t warn_brake_lamp_fuse : 1;
  uint8_t warn_abs             : 1;
  uint8_t                      : 1;
  uint8_t power_shift          : 1;
  uint8_t warn_overspeed       : 1;
  uint8_t cruise_arm           : 1;

  uint8_t cruise_act : 1;
  uint8_t prndl      : 3;
  uint8_t            : 1;
  uint8_t            : 1;
  uint8_t            : 1;
  uint8_t            : 1;

  uint8_t tcs        : 1;
  uint8_t            : 1;
  uint8_t            : 1;
  uint8_t            : 1;
  uint8_t            : 1;
  uint8_t            : 1;
  uint8_t buzzer     : 1;
  uint8_t            : 1;

  /* LED PWM control words */
  uint8_t bright_gauge;
  uint8_t bright_ptr;
  uint8_t bright_lcd;

  /* gauge control words */
  uint16_t speedo;
  uint16_t tacho;
  uint8_t  fuel;
  uint8_t  temp;

  uint8_t               : 3;
  uint8_t cluster_test  : 1;
  uint8_t lcd_test_pat1 : 1;
  uint8_t lcd_test_pat2 : 1;
  uint8_t               : 2;
} aldl_vy_cluster_ctrl_t;
#pragma pack()

#endif
