#ifndef _ALDL_MODELS_VY_BCM_H
#define _ALDL_MODELS_VY_BCM_H

#include <stdint.h>

#ifndef ALDL_LOCK_CMD
#define ALDL_LOCK_CMD

#define ALDL_UNLOCK_DRIVER 0x00
#define ALDL_UNLOCK_ALL    0x01
#define ALDL_LOCK_ALL      0x02
#define ALDL_LOCK_DEADLOCK 0x03

#endif

#pragma pack(1)
typedef struct {
  uint8_t : 8;

  /* discrete output enable word */
  uint8_t en_indicators    : 1;
  uint8_t en_illum_relay   : 1;
  uint8_t en_win_pass      : 1;
  uint8_t en_win_driver    : 1;
  uint8_t                  : 2;
  uint8_t en_win_relay     : 1;
  uint8_t                  : 1;

  uint8_t en_dome_light    : 1;
  uint8_t en_headlights    : 1;
  uint8_t en_ls_fan        : 1;
  uint8_t en_acc_relay     : 1;
  uint8_t en_horn          : 1;
  uint8_t en_rear_demister : 1;
  uint8_t en_ac_led        : 1;
  uint8_t en_wipers_front  : 1;

  uint8_t                   : 3;
  uint8_t en_security_led   : 1;
  uint8_t en_auto_headlight : 1;
  uint8_t en_theft_horn     : 1;
  uint8_t                   : 2;

  uint8_t : 8;

  uint8_t en_data_bus_isol : 1;
  uint8_t                  : 7;

  uint8_t : 8;

  /* discrete output state word */
  uint8_t indicators    : 1;
  uint8_t illum_relay   : 1;
  uint8_t win_pass      : 1;
  uint8_t win_driver    : 1;
  uint8_t               : 2;
  uint8_t win_relay     : 1;
  uint8_t               : 1;

  uint8_t dome_light    : 1;
  uint8_t headlights    : 1;
  uint8_t ls_fan        : 1;
  uint8_t acc_relay     : 1;
  uint8_t horn          : 1;
  uint8_t rear_demister : 1;
  uint8_t ac_led        : 1;
  uint8_t wipers_front  : 1;

  uint8_t                : 3;
  uint8_t security_led   : 1;
  uint8_t auto_headlight : 1;
  uint8_t theft_horn     : 1;
  uint8_t                : 2;

  uint8_t : 8;

  uint8_t data_bus_isol : 1;
  uint8_t               : 7;

  uint8_t en_dash_brightness : 1;
  uint8_t                    : 7;

  uint8_t dash_brightness;

  uint8_t en_lock                 : 1;
  uint8_t en_bootrel              : 1;
  uint8_t en_warn_park_lamp       : 1;
  uint8_t en_warn_brake_lamp      : 1;
  uint8_t en_warn_brake_lamp_fuse : 1;
  uint8_t                         : 3;

  uint8_t lock_cmd                : 2;
  uint8_t bootrel                 : 1;
  uint8_t warn_park_lamp          : 1;
  uint8_t warn_brake_lamp         : 1;
  uint8_t warn_brake_lamp_fuse    : 1;
  uint8_t                         : 2;
} aldl_vy_bcm_ctrl_t;
#pragma pack()

#endif
