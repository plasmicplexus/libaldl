#ifndef _ALDL_MODELS_VT_PCM_H
#define _ALDL_MODELS_VT_PCM_H

#include <stdint.h>

#pragma pack(1)
typedef struct {
  /* discrete output enable word */
  uint8_t en_shift_sol : 1;
  uint8_t              : 1;
  uint8_t en_hs_fan    : 1;
  uint8_t en_ls_fan    : 1;
  uint8_t en_cel       : 1;
  uint8_t en_ac_clutch : 1;
  uint8_t en_fuel_pump : 1;
  uint8_t              : 1;

  /* discrete output control word */
  uint8_t shift_sol_a : 1;
  uint8_t shift_sol_b : 1;
  uint8_t hs_fan      : 1;
  uint8_t ls_fan      : 1;
  uint8_t cel         : 1;
  uint8_t ac_clutch   : 1;
  uint8_t fuel_pump   : 1;
  uint8_t             : 1;

  /* injector output enable word */
  uint8_t en_inj_1 : 1;
  uint8_t en_inj_2 : 1;
  uint8_t en_inj_3 : 1;
  uint8_t en_inj_4 : 1;
  uint8_t en_inj_5 : 1;
  uint8_t en_inj_6 : 1;
  uint8_t          : 2;

  /* injector output control word */
  uint8_t inj_1 : 1;
  uint8_t inj_2 : 1;
  uint8_t inj_3 : 1;
  uint8_t inj_4 : 1;
  uint8_t inj_5 : 1;
  uint8_t inj_6 : 1;
  uint8_t       : 2;

  /* mode control enable word */
  uint8_t en_fuel_cl     : 1;
  uint8_t en_iac_cl      : 1;
  uint8_t en_ign_backup  : 1;
  uint8_t en_blm_reset   : 1;
  uint8_t en_iac_reset   : 1;
  uint8_t en_clear_dtc   : 1;
  uint8_t en_fuel_backup : 1;
  uint8_t                : 1;

  /* mode control state word */
  uint8_t fuel_cl : 1;
  uint8_t iac_cl  : 1;
  uint8_t         : 6;

  uint16_t : 16;
  uint32_t : 32;

  /* function modification control word */
  uint8_t en_iac        : 1;
  uint8_t iac_pos_rpm   : 1;
  uint8_t en_afr        : 1;
  uint8_t en_ign        : 1;
  uint8_t ign_abs_delta : 1;
  uint8_t ign_adv_ret   : 1;
  uint8_t               : 1;
  uint8_t en_egr        : 1;

  union {
    uint8_t iac;
    uint8_t rpm;
  };

  uint8_t afr;
  uint8_t ign;
  uint8_t egr;

  uint8_t  : 8;
  uint16_t : 16;

  /* discrete output enable word */
  uint8_t                  : 1;
  uint8_t en_start_inhibit : 1;
  uint8_t                  : 6;

  /* discrete output control word */
  uint8_t               : 1;
  uint8_t start_inhibit : 1;
  uint8_t               : 6;
} aldl_vt_pcm_ctrl_t;
#pragma pack()

#endif
