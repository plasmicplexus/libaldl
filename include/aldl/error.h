#ifndef _ALDL_ERROR_H
#define _ALDL_ERROR_H

/* ALDL errors */
#define ALDL_ESUCCESS     0
#define ALDL_EINVAL       1
#define ALDL_EINIT        2
#define ALDL_ENOPORT      3
#define ALDL_EIO          4
#define ALDL_ENORESP      5
#define ALDL_EBADRESP     6
#define ALDL_ENOMEM       7
#define ALDL_ENOSYS       8
#define ALDL_ETHREAD      9
#define ALDL_ETTYCONF     10
#define ALDL_ETTYOPEN     11
#define ALDL_EPCAPOPEN    12
#define ALDL_EBADVEHICLE  13
#define ALDL_ENODRIVER    14
#define ALDL_EMISSINGFEAT 15

const char *aldl_strerror(int error);

#endif
