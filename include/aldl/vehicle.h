#ifndef _ALDL_VEHICLE_H
#define _ALDL_VEHICLE_H

#include <stdint.h>

#include "aldl.h"

typedef struct {
  int   make;
  char *name;
  char *region;
} aldl_make_t;

typedef struct {
  aldl_cvid_t     vehicle;
  char           *model_name;
  char           *model_code;
  uint16_t        model_year;
} aldl_model_t;

int aldl_get_makes(const aldl_make_t **makes, size_t *nmakes);
int aldl_get_models(int make, aldl_model_t **models, size_t *nmodels);
int aldl_get_model(aldl_cvid_t modelid, aldl_model_t *model);

#endif
