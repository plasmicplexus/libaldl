#ifndef _ALDL_LOG_H
#define _ALDL_LOG_H

#include <aldl/aldl.h>

enum ALDL_LOG {
  ALDL_LOG_NONE = 0,
  ALDL_LOGV     = 1,
  ALDL_LOGI     = 2,
  ALDL_LOGW     = 3,
  ALDL_LOGE     = 4
};

#endif
