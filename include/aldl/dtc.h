#ifndef _ALDL_DTC_H
#define _ALDL_DTC_H

#include <stdint.h>

#include <aldl/aldl.h>

typedef struct {
  const char *description;
  const char *location;
  uint16_t    sae_dtc;
  uint16_t    oem_dtc;
  int         historical;
  aldl_time_t timestamp;
  aldl_cvid_t vehicle_module;
} aldl_dtc_t;

int aldl_dtc_read(aldl_session_t session, aldl_cvid_t module, aldl_dtc_t **dtc, size_t *ndtc);
int aldl_dtc_clear(aldl_session_t session, aldl_cvid_t module);

#endif