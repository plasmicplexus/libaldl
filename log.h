#ifndef _LOG_H
#define _LOG_H

#include "aldl.h"

enum ALDL_LOG {
  ALDL_LOG_NONE = 0,
  ALDL_LOGV     = 1,
  ALDL_LOGI     = 2,
  ALDL_LOGW     = 3,
  ALDL_LOGE     = 4
};

void ctos(char*, void*, size_t);
void pkttos(char*, aldl_packet_t *);

int logw(aldl_session_t *s, int level, char *subsys, char *fmt, ...);

int log_init(void);

#endif