#include <stdlib.h>
#include <string.h>

#include "aldl.h"
#include "driver.h"
#include "vehicle.h"

#define NMAKES \
  (sizeof(make_table) / sizeof(aldl_make_t))

static const aldl_make_t make_table[] = {
  {
    .make   = ALDL_MAKE_HOLDEN,
    .name   = "Holden",
    .region = "Australia/NZ"
  },
  {
    .make   = ALDL_MAKE_CHEVROLET,
    .name   = "Chevrolet",
    .region = "North America"
  },
  {
    .make   = ALDL_MAKE_PONTIAC,
    .name   = "Pontiac",
    .region = "North America"
  },
  {
    .make   = ALDL_MAKE_VAUXHALL,
    .name   = "Vauxhall",
    .region = "United Kingdom"
  }
};

APIEXPORT int aldl_get_makes(const aldl_make_t **makes, size_t *nmakes) {
  assert(!makes, ALDL_EINVAL);
  assert(!nmakes, ALDL_EINVAL);

  *makes = make_table;
  *nmakes = NMAKES;

  return 0;
}

APIEXPORT int aldl_get_models(int make, aldl_model_t **models, size_t *nmodels) {
  int i;
  int n = 0;

  aldl_model_t *s, *d;

  assert(!make || !models || !nmodels, ALDL_EINVAL);

  for(i = 0; i < ndriver; i++) {
    s = drivers[i]->models;
    while(s->vehicle) {
      if(aldl_make(s->vehicle) == make)
        n++;
      s++;
    }
  }

  assert(!n, 0);

  *models = malloc(n * sizeof(aldl_model_t));
  assert(!*models, ALDL_ENOMEM);

  d = *models;
  for(i = 0; i < ndriver; i++) {
    s = drivers[i]->models;
    while(s->vehicle) {
      if(aldl_make(s->vehicle) == make)
        memcpy(d++, s, sizeof(aldl_model_t));
      s++;
    }
  }

  *nmodels = n;
  return 0;
}

APIEXPORT int aldl_get_model(aldl_cvid_t vehicleid, aldl_model_t *model) {
  int i;

  aldl_model_t *m;

  assert(!aldl_make(vehicleid), ALDL_EINVAL);
  assert(!aldl_model(vehicleid), ALDL_EINVAL);
  assert(!model, ALDL_EINVAL);

  for(i = 0; i < ndriver; i++) {
    m = drivers[i]->models;
    while(m->vehicle) {
      if(m->vehicle == vehicleid) {
        memcpy(model, m, sizeof(aldl_model_t));
        return 0;
      }
      m++;
    }
  }

  return ALDL_EBADVEHICLE;
}
