SRCS = $(wildcard *.c) $(wildcard drivers/*.c)
HDRS = $(wildcard *.h) $(wildcard include/aldl/*.h)
OBJS = $(SRCS:.c=.o)

UNAME = $(shell uname)

ifeq ($(UNAME), Linux)
  TARGET  = libaldl.so
  PREFIX ?= /usr
  CFLAGS  = -lm -pthread -fvisibility=hidden -g -Wno-incompatible-pointer-types
  CC      = gcc $(CFLAGS)
else ifeq ($(UNAME), Darwin)
  TARGET  = libaldl.dylib
  PREFIX ?= /usr/local
  CFLAGS  = -pthread -fvisibility=hidden -g -Wno-incompatible-pointer-types -Wno-incompatible-function-pointer-types
  CC      = clang $(CFLAGS)
else
  $(error Unknown target operating system: $(UNAME))
endif

build: $(TARGET)

install: $(TARGET)
	install -d $(PREFIX) $(PREFIX)/lib $(PREFIX)/include
	install -m 755 $(TARGET) $(PREFIX)/lib/$(TARGET)
	strip -S $(PREFIX)/lib/$(TARGET)
	find include -type d -exec install -d $(PREFIX)/{} \;
	find include -type f -exec install -m 755 {} $(PREFIX)/{} \;

uninstall:
	rm -f $(PREFIX)/lib/$(TARGET)
	rm -rf $(PREFIX)/include/aldl

clean:
	rm -f $(OBJS)
	rm -f $(TARGET)

$(TARGET): $(OBJS)
	$(CC) -shared -o $(TARGET) $(OBJS)

.c.o:
	$(CC) -fPIC -c -o $*.o $^
