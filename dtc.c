#include "aldl.h"
#include "dtc.h"

APIEXPORT int aldl_dtc_clear(aldl_session_t *s, aldl_cvid_t module) {
  assert(!s->driver, ALDL_ENODRIVER);
  assert(!s->driver->dtc_clear, ALDL_EBADVEHICLE);

  return s->driver->dtc_clear(s, module);
}
APIEXPORT int aldl_dtc_read(aldl_session_t *s, aldl_cvid_t module, aldl_dtc_t **dtc, size_t *ndtc) {
  int i;
  int ret;

  aldl_dtc_t *dtclist;
  size_t ndtclist;

  aldl_time_t us = osal_micros();

  *dtc  = NULL;
  *ndtc = 0;

  assert(!aldl_module(module), ALDL_EINVAL);
  assert(aldl_make(module) || aldl_model(module), ALDL_EINVAL);

  assert(!s->driver, ALDL_ENODRIVER);
  assert(!s->driver->dtc_read, ALDL_EBADVEHICLE);

  ret = s->driver->dtc_read(s, module, &dtclist, &ndtclist);
  assert(ret, ret);

  for(i = 0; i < ndtclist; i++) {
    dtclist[i].timestamp      = us;
    dtclist[i].vehicle_module = module;
  }

  *dtc = dtclist;
  *ndtc = ndtclist;
  return 0;
}