# Purpose

`libaldl` is a library for communicating with ALDL-equipped GM vehicles.

Documentation is still a work in progress, but the basic API reference can be found on the [project wiki](https://gitlab.com/plasmicplexus/libaldl/-/wikis/API-Reference)

Please note, that the library is currently in the early stage of development, and is therefore likely to change. Primary API elements are close to being finalized, however some tweaking to your program might be required in future.

## Current Features
 - Reading DTCs
 - Clearing DTCs
 - GM Seed/Key Calculation

## Features (Planned)
Listed below are planned features by order of priority:
 - Reading and Interpreting Live Data (using Mode 1)
 - Control of PCM, BCM and Cluster and other modules using Mode 4 commands
 - Module linking/programming (VIN, radio pin, key transponders, etc)
 - Configuration of modules such as Cluster, BCM and more
 - Microcontroller support (such as the AVR8 under the [Arduino IDE](https://www.arduino.cc/))
 - Flashing PCM
 - Reading PCM RAM

# Building

### Windows

To build the library, simply load `libaldl.vcxproj` in Visual Studio, and build the project.

### Linux / macOS

To build the library, simply run the following command:

```
make
```

# Installing

To install the library on your system (including header files), simply run:

```
make install
```

To install the library to a custom location, run:

```
make install PREFIX=<dir>
```
